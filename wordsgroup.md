| NUM |Word | Translation |
| ------ | ------ | ------ |
| 1 | in terms of |就……而言；从……角度来看；就……方面而言 |
| 2 | be satisfied with |对……感到满意 |
| 3 | deal with |1．处理；应对：对引起问题或困境的人或事物采取行动 2．与某人做生意；与某人达成商业协议：与某人进行商业交易或达成协议。 3．关于某事物；涉及某事物：谈论、处理或讨论的主题。 4．接受；应对：接受或试图接受无法改变的事实。 | 
| 4 | in contrast to |与……形成对比；与……相反 |
| 5 | a member of |某个团体的成员：指某人属于某个组织、团体或团队的一员。 |
| 6 | in addition to |除了……之外：表示在已经提到的事物之外，还有额外的事物或观点。 |
| 7 | set great store |非常重视；高度评价 |
| 8 | root for |支持，资助（口语） |
| 9 | reach out  |伸出援手 |
| 10 | lecture on  |后面一般加学科名称,严肃的作品，分类的研究,一般是用于小范围、比较确切的知识点 |
| 11 | lecture in |以讲座的方式呈现,一般指的范围会大一点 |
| 12 | taking over |接管：指接替某人的职位或职责，或者控制某个组织、公司等 |
| 13 | taking over |接任，接管：接替某人或某个职位，承担责任或控制权 |
| 14 | step in |插手；介入：参与某个活动、问题或争论中，通常是为了提供帮助或解决问题。 |
| 15 | heats up |变热：指变得温暖或热。 变得更活跃、激烈或愤怒：指变得更加活跃、激烈或愤怒。 加热（某物）：指使（某物）变得温暖或热。 | 
| 16 | undivided attention |全神贯注，专心致志 |
| 17 | significance level |显著性水平：用来评估统计结果是否具有统计学上的显著性的一个指标。 |
| 18 | one side |adj.片面的，单方面的；不公正的 |
| 19 | my take on this |my opinion |
| 20 | to my way of thinking | |
| 21 | I'm in two minds(about this) | |
| 22 | I'm on the fence | |
| 23 | it boils down to  |关键点是 |
| 24 | it's the bees knees |it's perfact |
| 25 | I can take it or leave it |I don't mind, I'm indifferent |
| 26 | it's not my cup of tea |i don't like it |
| 27 | i can't make head or tail of it |没有头绪 |
| 28 | I can't get my head around it |不能思考 |
| 29 | none other than |不是别人，正是（表示吃惊） |
| 30 | run of the mill |普通 平凡无奇 |
| 31 | run into |遇到（run into problems 遇到问题） |
| 32 | it's not a patch on it's can't hold a candle to | it's not as good as |
| 33 | keen to do |热衷于做;热衷做; |
| 34 | narrow down   |缩小到...; 减少到… |
| 35 | look at sth |看，浏览，检查 |
| 36 | made up |补足; 拼凑 |
| 37 | setting up |设立；装置 |
| 38 | shore up |支撑 |
| 39 | due to |因为, 由于 |
| 40 | according to |根据 |
| 41 | meet all sorts |各种各样 |
| 42 | chop |n.  肋骨肉, 排骨; 切球, 削球; 砍, 剁, 劈; 切击#商标; 品质 v.  砍, 斩, 劈; 劈成; 砍成; 劈出; 砍, 猛击; 切球; 突然改变方向; 突然改变主意 | 
| 43 | chopping |切 |
| 44 | mince |n.  切碎物 v.  切碎, 矫揉做作地说; 碎步走, 装腔作势 | 
| 45 | properly |adv.  恰当地; 有礼貌地; 正确地; 体面地 |
| 46 | finely |adv.  精巧地; 细微地; 纤细地; 优雅地 |
| 47 | filling |n.  充填物, 填土, 填料 |
| 48 | stall |n.  货摊, 摊位; 小围栏, 马厩或牛棚里给一个动物的一小间, 隔间; 推迟的借口; 给一个手指的护套; (飞机用语) 失去控制, 无法起飞 v.  把...关入畜舍; 使动弹不得; 使陷入泥潭; 使熄火; 被关入畜舍; 抛锚; 熄火; 失速; 拖延, 推迟; 拖时间使等候, 把...拖住; 支吾, 拖延; 拖延时间 | 
| 49 | wrapping |n.  包装纸; 包装材料 |
| 50 | wrap |n.  外套, 包裹, 围巾 v.  包装, 缠绕, 卷; 缠绕, 穿外衣, 重叠 | 
| 51 | dough |n.  生面团; 钱, 现款; 似生面团的东西; 布丁 |
| 52 | eloquent |adj.  雄辩的, 有说服力的; 富于表现的 |
| 53 | shape |n.  外形, 形态, 形状 v.  使成形; 形成; 塑造, 制作; 使符合; 成形; 形成; 成型; 成长, 发展 | 
| 54 | afterward |adv.  之后, 后来, 以后 |
| 55 | capacity |n.  容量, 才能, 能力 |
