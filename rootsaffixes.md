| NUM |Word | Translation |
| ------ | ------ | ------ |
| 1 | ripe |v.成熟 |
| 2 | rip |n 裂口 |
| 3 | conrrupt |v 贿赂 a 腐败的 | 
| 4 | erupt |v 爆发（火山） |
| 5 | interrupt |v 打断 |
| 6 | disrupt |v 崩溃 |
| 7 | rupture |n 破裂 |
| 8 | | 9 | cry |v 哭，喊 |
| 10 | scream |v 尖叫 |
| 11 | claim |词根：叫 |
| 12 | proclaim |v 宣称 |
| 13 | exclaim |v 呼喊 |
| 14 | reclaim |v 要求归还 |
| 15 | acclaim |v 欢呼喝彩 |
| 16 | claim |v 要求声称 |
| 17 | | 18 | verse-vert |词根：旋转 |
| 19 | invert |v 倒置 |
| 20 | avert |v 躲避，转移 |
| 21 | convert |v 皈依；兑换 |
| 22 | divert |v 转向；兑换 |
| 23 | diversion |n 转向 |
| 24 | verse |旋转 |
| 25 | reverse |v 倒转 |
| 26 | adverse |a 不利的，有害的 |
| 27 | conversely |adv 相反 |
| 28 | conversation |n 对话 |
| 29 | verse |n 诗歌 |
| 30 | | 31 | one-uni | |
| 32 | universe |n 宇宙 |
| 33 | universal |a 宇宙的；普遍的 |
| 34 | university |n 大学 |
| 35 | | 36 | contro |相对 |
| 37 | extra |向外 |
| 38 | intra |向里 |
| 39 | controversial |a 引起争议的 |
| 40 | controversy |n 争议 |
| 41 | diverse |a 多种多样的 |
| 42 | diversity |n 多样性 |
| 43 | divorce |n 离婚 |
| 44 | version  |n 版本 |
| 45 | versatile |a 多才多艺；多功能 able- |
| 46 | versus |对抗 |
| 47 | inverse |a 相反的 |
| 48 | quiver |颤抖 |
| 49 | | 50 | sist-站 |词根 |
| 51 | subsistence |n 维持生存 |
| 52 | insist |v 坚决要求 |
| 53 | exist |v 存在 |
| 54 | existence |n 存在 |
| 55 | resist |v 抵制 |
| 56 | resistant |adj 抵制的 |
| 57 | resistance |n 抵制的 |
| 58 | transistor |n 晶体管 |
| 59 | consist |v 组成 （of） |
| 60 | consistent |adj 一致的（空间；前后（时间） |
| 61 | consist |in v 存在于 |
| 62 | persist |v 持久 |
| 63 | persistent |a 持久的 |
| 64 | stand |v 站 |
| 65 | assist |v 协助-chairman n主席 |
| 66 | assistant |n 助理 |
| 67 | assistance |n 协助 |
| 68 | | 69 | dictionary |n 字典 |
| 70 | dict-词根 |说 |
| 71 | contradict |v 反驳 |
| 72 | contradiction |n 反驳 |
| 73 | predict  |v 预测 |
| 74 | dictionary |n 语言 |
| 75 | dictate |v 命令 |
| 76 | dictator |n 独裁者 |
| 77 | indicate |v 暗示 表明 |
| 78 | indication |暗示表明 |
| 79 | indicator |n 指数 |
| 80 | indicative |a 表明的 |
| 81 | addict |n 上瘾的人 a 上瘾的 |
| 82 | addiction |n 上瘾  |
| 83 | verdict |v 裁定 |
| 84 | | 85 | doc-词根 |写下来 |
| 86 | document |n 文件，文档 |
| 87 | documentary |a 文件的 n 纪录片 |
| 88 | orthodox |n 传统的，正统的；东正教的 |
| 89 | paradox |n 悖论；矛盾 |
| 90 | | 91 | pose-词根 |放 |
| 92 | propose |v 建议；求婚 |
| 93 | proposal |n 建议；求婚 |
| 94 | proposition |n 建议 |
| 95 | suppose |v 假设 |
| 96 | supposal |n 假设 |
| 97 | deposit |v 沉淀；存款 |
| 98 | depot |n 储藏室 |
| 99 | impose |v 强迫；征税 |
| 100 | expose |v 暴露 |
| 101 | exposure |n 暴露 |
| 102 | compose |v 组成；zuowen；作曲 |
| 103 | composite |a 组成的 |
| 104 | composition |n 组成；作文作曲 |
| 105 | composure |n 沉着 |
| 106 | pose |n 姿势 v 摆放 |
| 107 | posture |n 姿势 |
| 108 | put |v 放 |
| 109 | dispost |v 处理；不值 |
| 110 | disposal |n 处理；不值 |
| 111 | disposable |a 可降解的，一次性的 |
| 112 | positive |a 去定的，积极地，肯定的 |
| 113 | pending |a 不确定的；待定 |
| 114 | position |n 位置 |
| 115 | opposition |n 反对 |
| 116 | | 117 | sess-词根 |坐 |
| 118 | possess |v 占有 |
| 119 | possession |n 占有；站有望 |
| 120 | | 121 | obsess |v 着迷，萦绕 |
| 122 | obsession |n 着迷 |
| 123 | purpose |n 目标 |
| 124 | preposition |n 介词（prep） |
| 125 | | 126 | spect-词根 |看 |
| 127 | spy |n 间谍 小偷 |
| 128 | inspect |v 检查 |
| 129 | inspector |n 检察院 |
| 130 | retrospect |v 回顾 |
| 131 | perspective |a 透视图 n 透视图；观点 |
| 132 | introspect |v 内省 |
| 133 | respect |v 尊敬，尊重 |
| 134 | respectable |a 可敬的 |
| 135 | respectful |a 毕恭毕敬的 |
| 136 | irrespective |a 不考虑的 |
| 137 | respective |a 各自的 |
| 138 | prospect |n 前景的 |
| 139 | spectator |n 观众 |
| 140 | spectable |n 奇观 ---miracle |
| 141 | spectacular |a 奇观的 |
| 142 | specimen |n 样品 calculate |
| 143 | speculate |v 推测，投机 |
| 144 | aspect |n 方面；外贸 |
| 145 | respect |v 尊敬，尊重 |
| 146 | suspect |v 怀疑 |
| 147 | suspicion  |怀疑 |
| 148 | suspicions |怀疑的 |
| 149 | sceptical |a 怀疑论的 怀疑主义的 |
| 150 | skeptical |a 怀疑论的 怀疑主义的 |
| 151 | despite |prep 尽管 |
| 152 | expect |v 期盼 |
| 153 | expected |a 期待的 |
| 154 | unexpected |a 始料未及的 |
| 155 | conspicuous |a 有目共睹的 |
| 156 | | 157 | press-压 | |
| 158 | pressure |压力 |
| 159 | suppress |v 镇压 |
| 160 | depress |v 压抑（情感） |
| 161 | impress |v 给谁深刻印象 |
| 162 | impressive |a 令人印象深刻的 |
| 163 | impression |n 印象 |
| 164 | express |v 表达 n 特快专递 |
| 165 | expression |n 表达 |
| 166 | expressive |a 表达的 |
| 167 | repress |v 一直的（情感） |
| 168 | compress |v 压缩 |
| 169 | compression |v 压缩 |
| 170 | | 171 | fact-词根 |做 factory |
| 172 | fect-词根 |做 |
| 173 | fict-词根 |做 |
| 174 | factory |n 制造厂 |
| 175 | factor |n 因素 |
| 176 | facile |a 容易做的事（对付事儿） |
| 177 | facility |n 容易 |
| 178 | facilitate |v 使容易 |
| 179 | facilitator |n 援助者 |
| 180 | fact |v 事实 |
| 181 | exact |v 确切的 |
| 182 | exactly |adv 事实地 |
| 183 | | 184 | fact | |
| 185 | fect-词根 |做 |
| 186 | infect |v 感染，传染 |
| 187 | infection |n 感染，传染 |
| 188 | infectionus |感染的 传染的 |
| 189 | effect |n 效果 结果  |
| 190 | effective |a 有效果的 |
| 191 | efficient |a 效率高的 |
| 192 | perfect | |
| 193 | affect |v 影响 |
| 194 | affection |n 疾病；感情 |
| 195 | suffice |v 足够 |
| 196 | sufficient |a 足够的 |
| 197 | deficit |n 亏损 |
| 198 | deficient |a 不足的 |
| 199 | deficiency |n 缺失 |
| 200 | efficient | |
| 201 | artificial |a 造作的；人造的 |
| 202 | profit |n 利益 |
| 203 | profitable |a 有利可图的 |
| 204 | | 205 | fer-词根 |运输携带 |
| 206 | ferry |v 摆渡 水上运输 |
| 207 | carry |v 运输 陆地运输 |
| 208 | prefer |v 更喜欢 |
| 209 | preference |n 更喜欢，偏好 |
| 210 | suffer |v 遭受（痛苦、疾病） |
| 211 | infer |v 推论 |
| 212 | inferior |a 次要的 |
| 213 | defer |v 屈尊；耽搁 |
| 214 | deference |n 屈尊；耽搁 |
| 215 | refer |v 体积，参考；refer to 指的是 |
| 216 | transfer |v 转学；转移；换乘；过户 |
| 217 | confer |v 交换意见，商讨 |
| 218 | conference |n 会议 |
| 219 | confer |v 授予 |
| 220 | differ |v 不同 |
| 221 | difference |n 不通 |
| 222 | interfere |v 打搅，干扰 |
| 223 | interference |n 干涉，干预 |
| 224 | intervene |v 干涉，干预 |
| 225 | | 226 | ferry |v 摆渡 水上运输 |
| 227 | fertile |a 能剩余的，肥沃的 |
| 228 | fertilize |v 是非 |
| 229 | fertilizer |n 肥料 |
| 230 | feirilization |n 施肥 |
| 231 | offer |v 提供 n 机会 |
| 232 | fer-pher（phor） | |
| 233 | metaphor |n 隐喻 |
| 234 | | 235 | tian-词根 |拿 |
| 236 | contain |v 容纳 |
| 237 | container |n 容器 |
| 238 | content |n 内容 a 满意的 |
| 239 | | 240 | take |v 拿 |
| 241 | sustain |v 支撑，支持 |
| 242 | sustainable |a 可制成的 |
| 243 | sustainability |n 可持续性 |
| 244 | detain |v 扣留；耽搁 |
| 245 | retian |v 保留 |
| 246 | retention |n 记忆，记忆力 |
| 247 | attain |n 达到（经过努力） |
| 248 | protein |v 蛋白质 |
| 249 | pertainb |v 有关 |
| 250 | pertinent |a 相关的 |
| 251 | entertain |v 招待；娱乐 |
| 252 | entertainment |n 娱乐 |
| 253 | | 254 | maintain |v 维持，持有观点 |
| 255 | maintenance |n 维持 |
| 256 | obtain |v 活的 = get |
| 257 | tennis |n 网球 |
| 258 | | 259 | scribe-写 |词根 |
| 260 | describe |v 描述、描写 |
| 261 | rub |v 擦、摩擦 |
| 262 | catch |v 抓 |
| 263 | scratch |v 挠 |
| 264 | prescribe |v 指示，固定；开处方 |
| 265 | prescription |n 厨房 |
| 266 | subscribe |v 签署同意；订阅；认捐 |
| 267 | subscription |n 订阅；认捐 |
| 268 | describe |v 描述描写 |
| 269 | description |n 表述 描写 |
| 270 | inscribe |v 题，刻 |
| 271 | transcribe |v 誊写 |
| 272 | transcription |n 誊写 |
| 273 | ascribe |v 归...于 |
| 274 | script |n 手稿 |
| 275 | | 276 | tect-词根 |盖子 |
| 277 | detect |v 发现 |
| 278 | detective |a 发现的 n 侦探 |
| 279 | detection |n 发现 |
| 280 | protect |v 保护 |
| 281 | | 282 | sect-词根 |切 |
| 283 | section |n 部分 |
| 284 | insect |n 昆虫 |
| 285 | infect |v 感染 |
| 286 | infest |v 滋生 |
| 287 | intersection |n 十字路口 inter相互 |
| 288 | sector |n 部门（公共部门 私有部门） |
| 289 | segment |n 片段 碎片 |
| 290 | segregate |v 隔离 |
| 291 | execute |n 执行 |
| 292 | executive |a 执行的 n 总经理 |
| 293 | execution |n 执行 |
| 294 | | 295 | plex-/plic-词根 |叠 |
| 296 | implicit |a 含蓄的 |
| 297 | explicit |a 坦率的 |
| 298 | complicate |v 复杂的 |
| 299 | complicated |a 错综复杂的 |
| 300 | perplex |v 使困惑 |
| 301 | surplus |过剩的 |
| 302 | replicate |v 折叠复制 |
| 303 | duplicate |v 折叠复制 |
| 304 | | 305 | suppy |v 提供供应 |
| 306 | supplier |n 供应商 |
| 307 | imply |v 暗示 含蓄 |
| 308 | implication |n 含义意义 |
| 309 | reply |v 回答 |
| 310 | comply |v 遵守 服从 |
| 311 | compliance |n 遵守 服从 |
| 312 | compliment |n 赞许 工位 |
| 313 | comment |n 评论 |
| 314 | | 315 | apply |v 申请 应用 |
| 316 | application |n 申请 应用 |
| 317 | applicable |a 可应用的 |
| 318 | | 319 | gress-词根 |行走 |
| 320 | progress |n 进步 |
| 321 | progressive |a 进步的 |
| 322 | regress |v 倒退；回归；退化 |
| 323 | regression |n 倒退；回归；退化 |
| 324 | congress |n 代表大会 |
| 325 | congressional |a 过会的 |
| 326 | aggressive |a 有上进心的 侵略的 |
| 327 | aggression |n 上进心 |
| 328 | trangress |v 越轨 违背道德 违犯（法律） |
| 329 | digress |v 立体 偏离主题 |
| 330 | | 331 | miss、mit 词根 |投 射 |
| 332 | nmd |national missile defense |
| 333 | missile |n 导弹 a 发射的 |
| 334 | premise |v 承诺 n 希望 |
| 335 | promising |a 有希望的 |
| 336 | compormise |v 拖鞋 损害 |
| 337 | submit |n 提交 投降 |
| 338 | transmit |v 转播 传送 |
| 339 | commit |v 委托 犯（罪） |
| 340 | emit |v 排放（废气） |
| 341 | intermittent |a 断断续续的 |
| 342 | committee |n 委员会 |
| 343 | submission |n 递交投降 |
| 344 | transmission |n 转播 传递 转发 |
| 345 | commission |n 委托（to） |
| 346 | emission |n 发射（废气） |
| 347 | dismiss |v 解散解雇 打发驳回 |
| 348 | missile |a 可投射的 n 导弹 |
| 349 | mission |n 使命 |
| 350 | missionary |n 传教士 |
| 351 | message |n信息 |
| 352 | messenger |n 信使 |
| 353 | | 354 | fess-词根 |说 |
| 355 | professor |n 教授 |
| 356 | profession |n 表白 宣布 文职 |
| 357 | professional |a 职业的 |
| 358 | confess |v 坦白 |
| 359 | phet-expert | |
| 360 | prophet |n 预言家 |
| 361 | prophecy |n 预言 |
| 362 | | 363 | form-词根 |形状、形式 |
| 364 | transform |v 变形，变化，变压 |
| 365 | transformer |n 变形者 |
| 366 | deform |v 使变形，破坏 |
| 367 | inform |v 通知 |
| 368 | informed |a 消息灵通的 |
| 369 | information |n 信息 |
| 370 | reform |v 改革 |
| 371 | conform |v 遵守 |
| 372 | perform |v 完成；表现；表演 |
| 373 | performer |n 表演者 |
| 374 | performance |n 表现；表演，性能 |
| 375 | unit n 单元 |united a 统一的 |
| 376 | uniform |n 制服 |
| 377 | formal |a 正式的 |
| 378 | formula |n 公式 |
| 379 | formulate |n 用诗表达；清晰的表达 |
| 380 | | 381 | voice voc-词根 |声音 |
| 382 | provoke |v 挑衅 招惹 激怒 |
| 383 | provocation |n 挑衅 招惹 激怒 |
| 384 | provocative |a 挑衅的 招惹的 |
| 385 | advocate |v 提倡 鼓吹 n 提倡者 |
| 386 | vocable |n 词 |
| 387 | vocabulary |n 词汇 |
| 388 | vocal |a 声音的 |
| 389 | vocation |n 召唤 天职 职业 |
| 390 | | 391 | vow-词根 |n 誓 誓言 誓约 |
| 392 | vowel |n 原因 |
| 393 | evoke |v 唤醒 |
| 394 | | 395 | visite |v 参观 |
| 396 | vis-、vid-词根 |看 |
| 397 | provide |v 提供 预备 假设 provided = if |
| 398 | provident | |
| 399 | provision |n 提供 假设 条款 |
| 400 | supervise |v 监督 监考 |
| 401 | survey |v 俯瞰 社会调查 n 社会调查 |
| 402 | surveillance |n 监控 -- economy 监督经济 |
| 403 | invisible |a 看不见的 |
| 404 | revise |v 修订 |
| 405 | preview |v 预习 |
| 406 | review |v 复习 |
| 407 | visit |v 参观 |
| 408 | visible |a 看得见的 |
| 409 | vision |v 视觉 |
| 410 | visitor |n 参观者 |
| 411 | visual |a 视觉的 |
| 412 | oral |a 口的 |
| 413 | aural |a 挺绝的 |
| 414 | manual |a 手的 |
| 415 | physical |a 身体的 |
| 416 | | 417 | visa |签证 |
| 418 | evidence |n 迹象 证据 |
| 419 | evident |a 清楚的，显然的 |
| 420 | advice |n 建议 |
| 421 | advise |v 建议 |
| 422 | evisage |v 正视 想象 |
| 423 | | 424 | duce-词根 |引导 |
| 425 | educate |v 教育 |
| 426 | introduce |v 介绍 引荐 |
| 427 | deduce |v 演绎 推到 |
| 428 | deduct |v 扣除 |
| 429 | deductible |a 可扣除的 |
| 430 | induce |v 导致 引导 |
| 431 | educate |v 教育 |
| 432 | reduce  |v减少 |
| 433 | reduction |n 减少 |
| 434 | conduct |n 行为 v 操作 |
| 435 | conductor |n 操作者 |
| 436 | semiconductor |n 半导体 |
| 437 | introduce |n 介绍 |
| 438 | introduction |n 介绍 |
| 439 | produce |生产 |
| 440 | product |n 产品 |
| 441 | production |n 生产 |
| 442 | productive |a 高产的 |
| 443 | reproduce |v 繁殖 |
| 444 | | 445 | flu-词根 |流 |
| 446 | flow |v 流 |
| 447 | superfluous |a 过剩的 |
| 448 | influence |n 渗透力 影响力 v影响 |
| 449 | influential |a 有影响力的 n ----人 |
| 450 | affluent |a 富足的 富裕的 n 富人 |
| 451 | fluent |a 流利的 |
| 452 | fluid |a 流体的 |
| 453 | solid |固体的 |
| 454 | liquid |液体的 |
| 455 | acid |酸性的 |
| 456 | fluctuate / ˈflʌktʃueɪt / |v 浮动 起伏 |
| 457 | | 458 | ject-词根 |投射 |
| 459 | project |v 肉社 投影 n 方案，项目 |
| 460 | subject |n 主题 主体 实验对象 v受制于 |
| 461 | subjective |v 主观的 |
| 462 | inject |v 注射  |
| 463 | eject |v 弹出 |
| 464 | reject |v 拒绝 |
| 465 | adjective |a 形容词 |
| 466 | | 467 | pel-词根、palmn 手掌 |推 |
| 468 | propel |v 推进 |
| 469 | propeller |n 推进器 |
| 470 | impel |v.推动，驱使 |
| 471 | expel |v.把……开除 驱逐 排出 |
| 472 | repel |v 击退 |
| 473 | compel |v 强迫 |
| 474 | compulsory / kəmˈpʌlsəri / |必须的 necessary a |
| 475 | | 476 | spir-词根 |呼吸 |
| 477 | spirit |n 精神 |
| 478 | inspire |v 稀奇；鼓励 |
| 479 | inspiration |n 灵感 |
| 480 | respire |v呼吸 |
| 481 | conspire |v 串通一气，合谋 |
| 482 | conspiracy |n 密谋 |
| 483 | perspire |v 排汗 |
| 484 | aspire |v 渴望 |
| 485 | aspiration |n 渴望 抱负 梦想 |
| 486 | seduce |v 诱奸 |
| 487 | | 488 | volve-词根 |旋转 |
| 489 | involve |v 卷入，牵涉 |
| 490 | evolve |v 发展 进化 |
| 491 | evolution |n 发展 进化 |
| 492 | revolt  |v 革命 反叛 |
| 493 | revolve |n 旋转；思考 |
| 494 | envelope |n 信封 |
| 495 | | 496 | stit-词根 |站 |
| 497 | stand |v 站 |
| 498 | substitute |v 代替 n 替代品 |
| 499 | institute |v 建立 n 学院 |
| 500 | institution |n 机构 制度 |
| 501 | institutional |a 机构的；制度的 |
| 502 | constitute |v 组成；制定法律 |
| 503 | constitution |n 宪法 |
| 504 | constitutional |a 宪法的 |
| 505 | constitutent |a 组成 n 选民 |
| 506 | | 507 | course |n 过程 v 快跑 |
| 508 | curs-、cur-词根 |跑 |
| 509 | current |n 气流 水流 电流 a 流通的 当前的 |
| 510 | currently |adv 档期啊的 |
| 511 | currency |n 货币 |
| 512 | occur |v 发生 |
| 513 | concur |v 同时发生 |
| 514 | recur |v 再次发生 |
| 515 | incur |v 招惹 |
| 516 | precursor / priˈkɜːrsər / |n 先驱 先兆 |
| 517 | excursion |n 远足 郊游 |
| 518 | | 519 | cede-、ceed-词根 |走 |
| 520 | precede |v 领先 |
| 521 | precedent |n 先例 |
| 522 | unprecedented |a 史无前例的 |
| 523 | preceding |a 领先的 |
| 524 | proceed |v 继续 |
| 525 | proceeding |n 急需的 |
| 526 | procedure |n 程序 |
| 527 | succeed |v 成功 |
| 528 | exceed |v 超出 |
| 529 | concede |v 让步，人数 |
| 530 | recede |v 减退 |
| 531 | | 532 | cess -词根 |走 |
| 533 | process |n 工序 过程 v 处理 |
| 534 | procession |n 处理 |
| 535 | success |n 成功 |
| 536 | successful |a 成功的 |
| 537 | succession |n 继承 |
| 538 | successive |a 继承的 |
| 539 | excess |n 超出 a 超出的 |
| 540 | excessive |a 超出的 |
| 541 | predecessor |n 前任 |
| 542 | recession |n 衰退 |
| 543 | access |n 入口 v 接近 |
| 544 | accessory |n 附件 同谋 |
| 545 | accessible |a 可达到的 |
| 546 | inacessible |a 不可达到的 |
| 547 | concession |n 让步 认输 迁就 |
| 548 | | 549 | sent-词根 |感觉 |
| 550 | thet-词根 |感觉 |
| 551 | essence |n 本质 |
| 552 | essential |a 本质的 必须的 |
| 553 | esthetic |a 感觉上的 |
| 554 | aesthetic |a 美学的 美的 |
| 555 | artistic |a 美学的 美感的 |
| 556 | resent |v 反感 怨恨 |
| 557 | consent |v 同感 |
| 558 | consensus / kənˈsensəs / |n 舆论 一致意见 |
| 559 | sense |n 感觉 理性 |
| 560 | sensible |a 感觉到的 |
| 561 | sensitive |a 感性的 |
| 562 | mental |a 精神的 |
| 563 | sentiment |n 情绪 |
| 564 | sentimental |a 多愁善感的 |
| 565 | scent |n 嗅觉 |
| 566 | | 567 | sess-词根 |坐 |
| 568 | sit |v 坐 |
| 569 | assess |v 评估 评价 |
| 570 | session |n 会议 |
| 571 | | 572 | cave |n 洞 |
| 573 | vac-词根 |空 |
| 574 | evacuate |v 撤空 |
| 575 | vacuum |n 真空 |
| 576 | vacant |a 空的 未被占用 |
| 577 | vacation |n 空闲 假期 |
| 578 | | 579 | receive |v 收到 |
| 580 | ceive-词根 |拿 |
| 581 | susceptible |a 易受影响的 |
| 582 | deceive |v 懵逼 欺骗 |
| 583 | deceit |n 懵逼 欺骗 |
| 584 | reception |n 招待 |
| 585 | concive |v 受孕 构思 |
| 586 | concept |n 概念 想法 |
| 587 | conception |n 概念的形成 |
| 588 | perceive |v 洞察 觉察 感知 |
| 589 | perceptin |n 洞察 察觉 感知 |
| 590 | | 591 | cip-词根 |拿 |
| 592 | receive |v收到 |
| 593 | recipient / rɪˈsɪpiənt / |n 获得者额a 收到的 |
| 594 | receipt |n 收据 发票 |
| 595 | reciprocal |a 相互的 互惠的 |
| 596 | mutual |a 相互的（情感上）n.互助公司 |
| 597 | disciple |n 信徒 门徒 追随者 |
| 598 | discipline |n 纪律 学科 |
| 599 | prince |n 王子 princess n 公主 |
| 600 | principal |a 首要的 n 规则 rule |
| 601 | principle |n 原则 |
| 602 | paricipate |v 分享 参与 |
| 603 | participant |a 参与的 n 参与者 |
| 604 | communicate |v 交流 |
| 605 | municipal |a 市的 市政的 |
| 606 | | 607 | sec-/sequ-词根 |跟着 |
| 608 | second |a 第二的，随后的 |
| 609 | prosecute |v 起诉 |
| 610 | prosecutor |n 检察官 |
| 611 | prosecution |n 起诉 诉讼 原告 |
| 612 | subsequent |a 随后的 |
| 613 | execute |v 执行死刑；执行 |
| 614 | execution |n 执行 |
| 615 | executive |a 执行的 n 总经理 |
| 616 | consequence |n  结果 意义 以你选哪个 |
| 617 | consequently |adv 结果 因此 |
| 618 | consecutive |a 连续的 |
| 619 | persecute |v 迫害 |
| 620 | sequence |n 秩序 |
| 621 | | 622 | right |a 正确的 直立的 |
| 623 | rect-词根 |直 |
| 624 | erect |v 勃起 竖起 |
| 625 | direct |a 直接的 |
| 626 | direction |n 方向 |
| 627 | director |n 总监导演 |
| 628 | rectify |v 纠正 |
| 629 | | 630 | sume-词根 |抓 摘 揪 |
| 631 | presume |v 假设 臆测 猜 |
| 632 | presumably |a 假设 |
| 633 | resume |n 简历 v 重新开始 | 
| 634 | buffet |n 自助餐 |
| 635 | recipe |n 食谱 |
| 636 | canteen |n 餐厅 |
| 637 | tornado |n 龙卷风 |
| 638 | tsunami |n 海啸 |
| 639 | seismic |a 地震的 |
| 640 | consume |v 消耗 吃完 和光 消费 |
| 641 | consumeption |n 消费 |
| 642 | assume |v 采取 承担 假设 |
| 643 | summary |n 摘要 |
| 644 | summarize |v 摘要 |
| 645 | | 646 | cise-、dide-词根 |切 |
| 647 | precise |a 精确的 精准的 |
| 648 | precisely |n 精确 |
| 649 | decide |决定 决断 |
| 650 | decisive |adj.果断的，决断的；决定性的，关键的 |
| 651 | decision |n.决定，抉择；判决，裁定；果断，决断力；作决定，决策 |
| 652 | excise |v 切除 |
| 653 | concise |a 简短的 |
| 654 | scissors |n 见到 |
| 655 | suiside |v 自杀 |
| 656 | pest |n 害虫 |
| 657 | pesticide |n 杀虫剂 |
| 658 | | 659 | serv-词根  |保存 |
| 660 | save |v 保存 |
| 661 | preserve |v 保鲜 保存 |
| 662 | deserve |v 应得 应受 |
| 663 | reserve |v 保留 预定 |
| 664 | reservation |n 保留 预定 |
| 665 | reservoir |n 水亏 |
| 666 | conserve |v 保存 保全 保守 |
| 667 | conservative |a 保守的、n 保守的 |
| 668 | conservation |n 保全 保守 |
| 669 | servant |n 努力 奴仆 |
| 670 | serve |v 服务 |
| 671 | service |n 服务 |
| 672 | | 673 | sid |做 |
| 674 | sit |v 坐 |
| 675 | preside |v 主持 |
| 676 | subside |v 平息 安抚 |
| 677 | subsidy |n 抚恤金 |
| 678 | subsidize |v 给抚恤金 |
| 679 | inside |adv 在里面 |
| 680 | insidious / ɪnˈsɪdiəs / |a 暗中为害 阴险的 隐袭的 |
| 681 | reside |v 居住 |
| 682 | resident |n 居民 |
| 683 | president |n 校长 行长 总统 |
| 684 | recidence |n 住所 |
| 685 | reside |v 居住 |
| 686 | | 687 | struct-词根 |建造 |
| 688 | destory |v 毁坏 |
| 689 | instruct |v 教授 |
| 690 | instructor |n 指导员 老师 |
| 691 | instruction |n 教育 |
| 692 | instructive |a 有教育意义的 |
| 693 | restructure |v 重建 |
| 694 | construct |v 建造 |
| 695 | construction |n 建筑 |
| 696 | structure |n 结构 |
| 697 | obstruct |v 阻碍 |
| 698 | | 699 | hibit-词根 |拿 |
| 700 | prohibit |v 禁止 |
| 701 | inhibit |v 抑制 |
| 702 | exhibit |v 展览 |
| 703 | exhibition |n 展览 |
| 704 | | 705 | cogn-词根 |知道 |
| 706 | recognize |v 认出 |
| 707 | cognition |n 认知学 |
| 708 | cognitive |a 认知的 |
| 709 | | 710 | gnor-词根 |知道 |
| 711 | | 712 | flat |a 平 |
| 713 | flect-、flex-词根 |弯曲 |
| 714 | flexible |a 灵活的 柔韧的 |
| 715 | reflect |v 反射 反向 反映 反思 |
| 716 | flat |a 扁平的 |
| 717 | | 718 | path-词根 |情感 |
| 719 | passion |n 激情 感情 |
| 720 | sympathy |n 同情 |
| 721 | sympathetic |a 同情的 |
| 722 | pathetic |a 可怜的 |
| 723 | | 724 | nounce-词根 |知道 |
| 725 | knows |v知道 |
| 726 | pronounce |v 宣告 发音 |
| 727 | pronunciation |n 发音 |
| 728 | prounounced |a 明显的 |
| 729 | denounce |v 公开抨击 |
| 730 | announce |v 宣告 发音 |
| 731 | | 732 | oper-词根 |做 |
| 733 | labor |n |
| 734 | operate |v 手术 操作 运作 |
| 735 | cooperate |n 合作 |
| 736 | cooperation |n 合作 |
| 737 | cooperative |a 合作的 |
| 738 | operate |v 操作 |
| 739 | operation |n 操作 |
| 740 | operational |a 操作的 |
| 741 | | 742 | sert--词根 |插入 |
| 743 | insert |n 插入 |
| 744 | desert |v 遗弃 |
| 745 | desert |n 沙漠 |
| 746 | assert |v 断言 宣城 |
| 747 | exert |v 施加（影响力） |
| 748 | | 749 | chron-词根 |时间 |
| 750 | clock |n 钟 表 |
| 751 | chronic |a 慢性的，积习难改的 |
| 752 | chron-article | |
| 753 | chronicle |n 编年史 历时 |
| 754 | patronize |v 摆出高人一等的派头 |
| 755 | | 756 | chronology | |
| 757 | chronological |a 年代的 |
| 758 | chronological error |穿帮了 |
| 759 | synchronize |n 同步（声音 画） |
| 760 | | 761 | mens-、mend-词根 |测量 |
| 762 | metre |n 米 v 测量 |
| 763 | immense |a 巨大的 |
| 764 | dimension |n 维度 |
| 765 | | 766 | come man！ | |
| 767 | command |v 命令 |
| 768 | mand-词根 |命令 |
| 769 | demand |v 要求 n 需求 |
| 770 | mandate |v 命令，授权 = command |
| 771 | | 772 | tract-词根 |拉扯 |
| 773 | drag |v 拽 |
| 774 | tractor |n 拖拉机 |
| 775 | distract |v 分散注意力 |
| 776 | portray |v 绘画 |
| 777 | portrait |n 肖像 |
| 778 | trait |n 特征 |
| 779 | subtract |v 扯走 减去 |
| 780 | extract |v 拔出 |
| 781 | retreat |v 撤退 退却 |
| 782 | contract |v 达成共识 收缩 感染 n合同 |
| 783 | distract |v 分散注意力 |
| 784 | distress |v 使痛苦 （五马分尸） |
| 785 | attract |v 吸引 |
| 786 | attraction |n 吸引力 |
| 787 | attractive |a 有吸引力的 |
| 788 | tractor |n 土垃圾 |
| 789 | trigger |n 扳机 v 触发 |
| 790 | strict |a 严格的 严厉的 |
| 791 | | 792 | port |n 港口 |
| 793 | port-词根 |运输 |
| 794 | support |v 支持 |
| 795 | deport |v.驱逐（非本国居民）出境 |
| 796 | import |v 进口 |
| 797 | report |v报道 |
| 798 | reporter |n 记者 |
| 799 | transport |v 运输 |
| 800 | porter |n 半运动 |
| 801 | portable |a 便携的 |
| 802 | export |v 出口 |
| 803 | | 804 | liber-词根 |自由 |
| 805 | illiberal |a 狭隘的 |
| 806 | liberal |a 自由的 |
| 807 | liberate |n 使自由 解放 |
| 808 | deliberate |a 蓄意的 |
| 809 | liberty |n 自由 |
| 810 | | 811 | cred-词根 |信 |
| 812 | incredible |a 难以置信的 |
| 813 | credible |a 可信的 |
| 814 | credit |n 信誉 学分 |
| 815 | creditor |n 债主 债权人 |
| 816 | debtor |n 债务 |
| 817 | confidential |a 亲信的 自信的 |
| 818 | credentials |n 证书 |
| 819 | | 820 | her-、hes-词根 |粘 |
| 821 | conherent |a 连贯的 |
| 822 | cohesive |a 凝聚力 |
| 823 | adhere |v 黏贴 坚持 |
| 824 | stick |v 黏贴 坚持 |
| 825 | hesitate |v 犹豫 |
| 826 | | 827 | pet-词根 |喜爱 |
| 828 | pet |n 宠物 爱畜 |
| 829 | compete |v 竞争 |
| 830 | competent |a 有能力的 |
| 831 | competitive |adj.竞争的；好竞争的，好胜的；有竞争力的 |
| 832 | competition |n 竞争 |
| 833 | petal |n 花瓣 |
| 834 | perpetual |a 四季开花的 永久的 |
| 835 | perpetuate |v 使永久 |
| 836 | petition |n 请愿 请愿书 |
