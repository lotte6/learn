| NUM |Word | Translation |
| ------ | ------ | ------ |
| 1 | ab |远离 |
| 2 | dome |圆屋顶 |
| 3 | abdomen |n 腹部，下腹 |
| 4 | domenant |凸出来，占优势的 |
| 5 | dominate |占优势 |
| 6 | predominate |占绝对优势的 |
| 7 | predominant |a 占绝对优势的 |
| 8 | abite |忍受 |
| 9 | obey |顺从 |
| 10 | obedient |顺从的 |
| 11 | obedience |n 顺从 |
| 12 | abnormal |不规则的 反常的 |
| 13 | norm |标准 规范 |
| 14 | nor |也不 |
| 15 | abolish |v 废除（法律、习惯） |
| 16 | establish |v 设立、建立 |
| 17 | absolete |a 废除的，过时的 |
| 18 | abolition |n 废除 |
| 19 | adolescent |n 青少年 a青春期的 |
| 20 | infant |婴儿 |
| 21 | junior |少年 |
| 22 | odd |a 奇数的，单只的 |
| 23 | union |n 联合，合并，银联 |
| 24 | abound |v 大量存在 |
| 25 | abundant |n 丰富的，大量的 |
| 26 | abundance |a 丰富的，大量的 |
| 27 | redundant |a 冗余的 |
| 28 | absorb |吸收 （抽象） |
| 29 | sort |吸收（水） |
| 30 | soup |汤 |
| 31 | abstract |拔出 |
| 32 | extract |抽出 |
| 33 | tract |拖拽 |
| 34 | drag |拽 |
| 35 | attract |吸引 |
| 36 | attractive |吸引 |
| 37 | subtract |减少 |
| 38 | abstract |adj.抽象的，纯概念的；（艺术）抽象派的；理论上的，不切实际的 n.摘要，梗概；抽象画，抽象派艺术作品；抽象的概念 v.抽象化，从理论上（或孤立地）考虑；提取，使分离；写……的摘要；偷走，窃取；退出 | 
| 39 | tractor |拖拉机 |
| 40 | trigger |n 扳机 v 触发 |
| 41 | absurd |荒谬的 荒诞的 不合理的 |
| 42 | surd |adj.不尽根的；清音的；无道理的 n.清音；不尽根；无理数 | 
| 43 | academic |a.学院的，大学的；理论的 |
| 44 | academy |学校 |
| 45 | CAS |中科院 |
| 46 | university |大学 |
| 47 | universe |宇宙 |
| 48 | college |学院 |
| 49 | band |布带 |
| 50 | bound |绑缚的 |
| 51 | bound |分界线 |
| 52 | boundary |n 分界线，边界 |
| 53 | abrupt |a 突然的，出其不意的 |
| 54 | rupture |破裂 |
| 55 | bankrupt |破产 |
| 56 | erupt |爆发 |
| 57 | corrupt |a腐败 v 贿赂 |
| 58 | interrupt |打扰 |
| 59 | rip |v 撕，裂 |
| 60 | spit |吐痰 |
| 61 | absolute |绝对的，完全的 |
| 62 | absolutely |绝对地，完全地 |
| 63 | sole |唯一的 |
| 64 | consolidate |v 使加固 （把…联合） |
| 65 | solid |固体的 |
| 66 | liquid |液体的 |
| 67 | ate |后缀 |
| 68 | ify |后缀 |
| 69 | ish（ise） |后缀 |
| 70 | strong |强 |
| 71 | strength |力量 |
| 72 | strengthen |v.使加强 |
| 73 | lighten |v 使发光 |
| 74 | shorten |v 缩短 |
| 75 | solemn |a 庄严的，庄重的 |
| 76 | solidarity |n 团结 |
| 77 | solitary |a 孤独的；偏僻的 |
| 78 | solo |n 独奏；a 单独的 |
| 79 | radio |收音机 |
| 80 | audio |音频 |
| 81 | video |视频 |
| 82 | piano |钢琴 |
| 83 | peninsula |n 半岛 |
| 84 | pan |盘底；前缀：全 |
| 85 | pen |前缀：半 |
| 86 | self+one=sole |唯一的 |
| 87 | isolate |隔离 |
| 88 | insulate |n 隔离，使孤立；绝缘 |
| 89 | desolate |荒凉的 |
| 90 | isl-词根 |隔离 |
| 91 | aisle /ail/ |n 过道 |
| 92 | island |n 岛屿 |
| 93 | isle |n 小岛 |
| 94 | absorb |vt 吸收；使全神贯注 |
| 95 | sorb |吸收 |
| 96 | soup |汤；可吸的东西 |
| 97 | advice |n 建议忠告 |
| 98 | advise |v 建议忠告 |
| 99 | abstract |a 抽象 n 摘要 v 提取 |
| 100 | extract |抽出 |
| 101 | tract-词根 |拉扯 |
| 102 | drag |拽 |
| 103 | tractor |拖拉机 |
| 104 | attract |v 吸引 |
| 105 | attractive |有吸引力的 |
| 106 | subtract |减少 |
| 107 | trigger |n扳机 v触发 |
| 108 | tractor |垃圾 |
| 109 | absurd |a荒谬的，荒诞的；不合理的 |
| 110 | accelerate |v 使加速，促进 |
| 111 | decelerate |速度 |
| 112 | celerity |快速 |
| 113 | construct |建造 |
| 114 | destruct |毁坏 |
| 115 | ascend |上升 |
| 116 | descend |下降 |
| 117 | velocity |n 速度，速率 |
| 118 | wheel |轮子 |
| 119 | access |n 进入；接入；到达 |
| 120 | success |继续；成功 |
| 121 | victory |胜利 |
| 122 | concession |n 让步拖鞋；特许 |
| 123 | concede |v 让步 |
| 124 | precede |v 领先 |
| 125 | proceed |继续；往前 |
| 126 | cease |v/n 停止，终止 |
| 127 | ceed、cess | |
| 128 | stop |暂停（设定好的） |
| 129 | pause |暂停（未设定好的） |
| 130 | end=cease |结束 |
| 131 | acclaim |v 向。。。欢呼 n欢呼喝彩 |
| 132 | shriek |v尖叫 |
| 133 | scream |v尖叫 |
| 134 | proclaim |宣城 |
| 135 | exclaim |呼喊求救 |
| 136 | acclaim |喝彩，欢呼 |
| 137 | reclaim |要求偿还；开垦荒地 |
| 138 | accommodate |v 向…提供住处（方便） |
| 139 | commodity |n 日用品 |
| 140 | convenient | |
| 141 | accomplish |v 达到；完成 |
| 142 | complete |n 完整的，完全的 |
| 143 | ample |a 多的 |
| 144 | complement |n 补足物，穿上的定员 |
| 145 | completion |n 完成，补齐 |
| 146 | supplement |n 补遗，增刊，附录 v 增刊 | 
| 147 | accumulate |v 堆积，累积 |
| 148 | clear |清楚的 |
| 149 | blear |模糊的 |
| 150 | blur |模糊 |
| 151 | same | |
| 152 | simulate |模拟 |
| 153 | ul |连字符 |
| 154 | sting |刺 |
| 155 | stimulate |v 刺激 |
| 156 | cumulous |a 积云状的 |
| 157 | cumulus |n 积云 堆积 |
| 158 | accuracy |n 精确 |
| 159 | accurate |a 精确地 |
| 160 | ac- |一再地 |
| 161 | cur（care） |用心 |
| 162 | secure |n 使安全 |
| 163 | see + care | |
| 164 | rescure |v 营救 |
| 165 | curb |n 路边 v 制止，抑制 | 
| 166 | curve |n.  曲线, 曲球, 弯曲 v.  成曲形; 弯; 使弯曲 | 
| 167 | carve |v 砍 |
| 168 | scar |n.  疤; 损伤痕; 伤痕; 创伤#断崖, 峭壁; 岩礁 v.  在...留下疤痕; 损伤; 使变丑; 伤害; 结疤; 留下伤痕; 愈合 | 
| 169 | sneeze |打喷嚏 nose + slim + line |
| 170 | current |n.  流动, 气流, 水流; 趋势, 潮流, 倾向; 电流 adj.  流行的；现行的, 当前的;  | 
| 171 | torrent（turn） |n.  急流, 连发, 洪流 |
| 172 | excursion |n.  远足; 短程旅行; 游览 |
| 173 | precursor |n.  先驱者, 先进者, 前导 |
| 174 | incur |v 招惹 |
| 175 | occur |v.  发生, 出现 |
| 176 | recur |v.  再发生（不好的事） |
| 177 | course(course) |过程；课程；路程；跑道 |
| 178 | curriculum |n 课程 |
| 179 | corridor |n 走廊，通路，通道 |
| 180 | cor 走 | |
| 181 | accuse |v 控告，职责 |
| 182 | excuse |原谅 |
| 183 | curse |n 诅咒，咒语 |
| 184 | cross |十字架 |
| 185 | damn |n.  诅咒; 丝毫, 一点点 int 该死 a 十足的 ad 十足的 v.  谴责；使失败 | 
| 186 | condemn |v.  判刑, 处刑, 责备 |
| 187 | appropriate |a 适当的，恰当的 vt 拨给，挪用，盗用 | 
| 188 | proper |adj.  适当的, 专属的, 高尚的 |
| 189 | private |n.  士兵, 列兵; 私下, 不公开; 阴部 adj.  私人的, 私立的, 秘密的 | 
| 190 | ache |n 疼痛 v 疼痛；爱莲 | 
| 191 | agony |n （精神或肉体）极大痛苦，创伤 |
| 192 | ag-词根 |动 |
| 193 | agitate |v 摇动，使焦虑不安 |
| 194 | act |行动 |
| 195 | instinct |n.  本能, 直觉 |
| 196 | colleague |n 同事 |
| 197 | collect |v 收集 |
| 198 | league |n.  同盟, 联盟; 联合会, 社团; 盟约; 俱乐部#里格; 为长度单位 v.  结盟; 联合; 使结盟; 使联合 | 
| 199 | ac |尖 |
| 200 | acid |n.  酸; 讥刺; 有酸味的东西; 迷幻药 adj.  酸的, 有酸味的; 尖酸刻薄的; 酸性的; 敏锐的; 讽刺的 | 
| 201 | solid |n.  固体; 立方体; 固态物; 固体非流质食物 adj.  固体的, 坚固的, 实心的 | 
| 202 | liquid |n.  液体; 流音; 流体 adj.  液体的; 透明的; 清澈的 | 
| 203 | angle |角 |
| 204 | acute |a 各种尖 |
| 205 | quain-词根 |任 |
| 206 | cogn |认 |
| 207 | acquaint /ə'kweɪnt/ |v.  介绍, 使认识; 使了解; 使熟知 |
| 208 | acquaintance |n.  相识; 熟人, 相识的人; 了解 |
| 209 | ignoran |n.  无知; 不知 |
| 210 | cognitive |adj.  认知的; 有感知的; 认识的 |
| 211 | recognize |v.  认出, 识别; 正式承认; 认识; 认可, 认定 |
| 212 | ignorant |adj.  无知识的, 幼稚的, 不知道的 |
| 213 | ignore |v 不理不顾 |
| 214 | adapt |v 使适应 |
| 215 | apt |适合的 |
| 216 | adept |adj.  熟练的, 内行的; 拿手的 n.  内行, 老手, 擅长者; 专家 | 
| 217 | expert |专家 |
| 218 | artist |艺术家 |
| 219 | experience |经验 |
| 220 | experiment |实验 |
| 221 | deprive |v 剥夺 |
| 222 | privilege |n.  特权, 基本人权, 特别恩典 v.  给与...特权, 特免 | 
| 223 | private |a 私人的 |
| 224 | adopt |v.  采取; 吸收; 采纳; 过继, 收养 |
| 225 | opt-词根： |选择 |
| 226 | option |选择 |
| 227 | optionical |adv 选择性的 |
| 228 | addict |n.  入迷的人, 有瘾的人; 上瘾者 v.  使沉溺, 使醉心; 使上瘾 | 
| 229 | adequate |adj.  足够的 |
| 230 | equal |相等的 |
| 231 | equation |均衡，等式 |
| 232 | equator |赤道 |
| 233 | eco-前缀 |相等 |
| 234 | ecology |生态学 |
| 235 | ecosystem |生态系统 |
| 236 | adhere |vi 粘着；坚持，遵守 |
| 237 | her |粘 |
| 238 | coherent |adj.  互相密合着的, 连贯的, 凝聚性的 |
| 239 | cohesive |adj.  粘着的 |
| 240 | hesitate |v.  犹豫, 踌躇, 迟疑 |
| 241 | | 242 | adjacent |a （to）（时间上）紧接着的，邻近的 |
| 243 | join |加入；链接 |
| 244 | joint |连接点；关节 |
| 245 | junction |结合，连接（抽象） |
| 246 | adjacent |adj.  毗连的, 邻近的, 接近的 n.  近邻, 同另外的装置通过直接连接联在一起的装置 (计算机用语) | 
| 247 | adjoin |v 邻近，靠近；贴近 |
| 248 | conjunction |n.  连接词, 关联, 联合 |
| 249 | jungle |n 丛林，生死地带 |
| 250 | saint |n.  圣人, 圣徒, 道德崇高的人 v.  承认为圣徒, 使成为圣徒 | 
| 251 | sane |a 精神健全的 |
| 252 | insane |a 愚蠢的（有精神病的） |
| 253 | insanity |a 精神错乱的 |
| 254 | sanction |圣人 |
| 255 | administer |v.  管理; 经营; 掌管; 实施, 执行;  |
| 256 | minister |n 部长；管家 |
| 257 | manage |v 管理 |
| 258 | manager |n 经理 |
| 259 | premire |n 总理 |
| 260 | president |n 总统 |
| 261 | illiterate |a 文盲的 |
| 262 | literate |认字的 |
| 263 | letter |字母 |
| 264 | literacy |n 有文化的，有教养的 |
| 265 | literally |adv.  逐字地, 照字面地; 实在地, 不加夸张地; 正确地; 简直 |
| 266 | literal |adv. 字面上的 |
| 267 | advance |n.  前进; 增长; 发展; 增高 v.  使向前移动; 将...提前; 推进, 促进; 预付; 前进; 进展; 向前移动; 进步 adj.  先行的; 预先的, 事先的 | 
| 268 | ad-vent-走-ance-词根：前 | |
| 269 | ancient |古老的 |
| 270 | ancestor |祖先 |
| 271 | advanced |先进的 |
| 272 | advantage |优势 |
| 273 | disadvantage |劣势 |
| 274 | advent |n.  出现; 到来 |
| 275 | adventure |n.  冒险, 冒险精神; 激动人心的活动; 冒险活动; 投机活动 v.  冒险去做; 大胆说出; 使冒险; 冒险 | 
| 276 | venture |n.  冒险, 冒险精神; 激动人心的活动; 冒险活动; 投机活动 v.  冒险去做; 大胆说出; 使冒险; 冒险 | 
| 277 | convention |n 大会；公约；协定；惯例 |
| 278 | glitter |n.  灿烂, 辉耀, 闪烁 v.  灿烂, 辉耀, 闪烁 | 
| 279 | glass |玻璃 |
| 280 | imitate |v 模仿 |
| 281 | imitative |a 模仿的 |
| 282 | imagine |想象 |
| 283 | adore |v 崇拜，敬仰 |
| 284 | dear |衍生出 |
| 285 | appreciate |v 为。。表示感激 |
| 286 | worship |n.  崇拜, 尊敬, 礼拜 v.  崇拜, 尊敬; 敬神, 拜神 | 
| 287 | worth-shape |价值-形状 |
| 288 | ventilate |v 使通风的 |
| 289 | ventile |a 通风的 |
| 290 | dvent |通风口 |
| 291 | went | |
| 292 | wind | |
| 293 | window | |
| 294 | vent-词根： |走 |
| 295 | avnenue |n 林荫路，大街 |
| 296 | street |没有树 |
| 297 | road |小路 |
| 298 | lane |小巷（line） |
| 299 | revenue |财政收入 |
| 300 | income |收入（家庭 |
| 301 | advocate |n 辩护者，拥护者 v 拥护 |
| 302 | voc词根 |声音 |
| 303 | voice |声音 |
| 304 | vocable |词 |
| 305 | vocabulary |词汇 |
| 306 | vocation |n 召唤；天职才能 |
| 307 | career |n 职业生涯；车辙；履历 |
| 308 | car+wheel | |
| 309 | profession |文职工作 |
| 310 | professor |教授 |
| 311 | vacation |假期 |
| 312 | voc-词根 |空 |
| 313 | vacant |空的 |
| 314 | vacuum |真空 |
| 315 | um |物理 |
| 316 | ium |化学 |
| 317 | space |空间 |
| 318 | provoke |v 挑衅；招惹；激发 |
| 319 | evoke |v 唤醒 |
| 320 | wake |v 叫醒 |
| 321 | voice-voc/vok | |
| 322 | aesthetic |a 美学的，艺术的 |
| 323 | esthetic |a 感觉得 |
| 324 | es=ex | |
| 325 | thet-词根 |感觉 |
| 326 | ic-的 | |
| 327 | sense | |
| 328 | sent-词根 |感觉 |
| 329 | consent |同感 |
| 330 | resent |方案 |
| 331 | sentiment |感觉，情感 |
| 332 | consensus |n 一致同意；舆论 |
| 333 | sense+sent+thet | |
| 334 | affuent |a 富裕的，富有的 （褒义的） |
| 335 | flu-flow | |
| 336 | flu-词根 |流 |
| 337 | influence |n.  影响; 势力; 感化 v.  影响; 改变 | 
| 338 | fluctuate |v 使波动 |
| 339 | flect-词根 |弯 |
| 340 | flection |弯曲 |
| 341 | inflect |使弯曲 |
| 342 | reflect |反应 |
| 343 | flexible |可弯曲的；柔韧的；灵活的 |
| 344 | superfluous |a 多余的，过剩的（贬义词） |
| 345 | afford |v 负担得起，买得起 |
| 346 | effort |n 努力 成就 |
| 347 | force |力量，强制 |
| 348 | forge |v 锻造 n 段工车间 |
| 349 | advice |n 忠告 |
| 350 | advise |v 忠告 |
| 351 | graze |v 吃草 放牧；擦伤 |
| 352 | grass |n 草 |
| 353 | aggravate |vt 家中，使恼火 |
| 354 | gravity |重力 |
| 355 | grave |坟墓；庄重的 |
| 356 | gray |灰色的 |
| 357 | grav-词根 |重力 |
| 358 | alleviate |减轻 |
| 359 | grief |n 悲伤，悲痛 |
| 360 | album |n 集邮册，相册 |
| 361 | alb-词根 |白 n.  白麻布圣职衣 |
| 362 | alps |阿尔卑斯山 |
| 363 | candid |adj 无偏见的，公正的，清白的，直率的 |
| 364 | candle |蜡烛 |
| 365 | candy |糖 |
| 366 | cand |白 |
| 367 | candidate |n 候选人，候补者 |
| 368 | alcohol |n 酒精 |
| 369 | coal |煤 |
| 370 | ert-词根：（erg） |动 |
| 371 | alert |a 机警的 竟觉得 vt 使警觉 |
| 372 | inert |无活动的，惰性的，迟钝的 |
| 373 | inertia |懒惰症 |
| 374 | energy |能量，使动 |
| 375 | enlarge |使增大 |
| 376 | artery |n 动脉 干线 |
| 377 | vein |静脉 |
| 378 | parent | |
| 379 | pair-词根 | |
| 380 | ar=air |空气 |
| 381 | al-词根 |外 |
| 382 | alien |n 外侨 外国人；外星人 a外国的；相异的 |
| 383 | alter |v 变更 |
| 384 | alternate |a 交替的，轮流的 |
| 385 | alternative |n 二选一的，取舍 a 二选一的 |
| 386 | ut/ult |出 |
| 387 | utter |v 说出，发出生意 a彻底的，完全的 |
| 388 | allege |v 断言，宣称 |
| 389 | legend |传说传奇 |
| 390 | ent、leg-词根 |说 |
| 391 | dialogue |对话 |
| 392 | lecture |讲座 |
| 393 | neglect |v、n 忽视；疏忽 |
| 394 | negate |否定 |
| 395 | negative |否定的；消极的 |
| 396 | negligible |a 可以忽略的，微不足道的 |
| 397 | alleviate |v 减轻，缓和 |
| 398 | lev-词根 |轻，举（leaf） |
| 399 | elevate |举高，举起 |
| 400 | elevator |电梯 |
| 401 | levy |n 、v征收，征税，征兵 |
| 402 | alliance |n 同盟，同盟国 |
| 403 | ally |结盟 |
| 404 | all |全部 |
| 405 | coalition |n 结合体，同盟，结合 |
| 406 | rally |v 重整，恢复 n 聚集，集合，拉力赛 |
| 407 | alloy |n 合金；捆绑 v 将...铸成 |
| 408 | alphabet |n 字母表 |
| 409 | vocablulary |词汇表 |
| 410 | altitude |n 海拔高度； |
| 411 | longitude |经度；经线 |
| 412 | latitude |纬度；纬线 |
| 413 | alt-词根 |高 |
| 414 | altai |阿尔泰山 |
| 415 | lat |拉扯 |
| 416 | aluminium   |n.  铝, 一种金属 (又拼作 aluminum) |
| 417 | ium | |
| 418 | lumen |流明 |
| 419 | luna |月亮女神 |
| 420 | luminous |发光的；明亮的 |
| 421 | illuminate |照明；阐明 |
| 422 | lumin-词根 |发光 |
| 423 | amateur |a、n 业余 |
| 424 | connoisseur |n 内行，鉴赏家 |
| 425 | amiable |和蔼可亲的；友善的 |
| 426 | ambassador |n 大使，特使，组织代表 |
| 427 | minister |大臣 |
| 428 | amb-词根 |走 |
| 429 | ass |驴，笨人，臀部 |
| 430 | embassy |大使馆 |
| 431 | ministry |部 |
| 432 | ambulance |战时流动医院 |
| 433 | amble |vi 缓行 |
| 434 | ambiguous |a 引起歧义的；模糊的，含糊不清的 |
| 435 | about |附近，大约左右 |
| 436 | ambition |n 对（成功、权利的）欲望，野心 |
| 437 | analysis |n分析 |
| 438 | analytic |a 分析的;分解的 |
| 439 | analyse |v 分析；分解 |
| 440 | apart |分开的 |
| 441 | ana-前缀 |分开的 |
| 442 | loose |v & a 松开；解开； 松，解 |
| 443 | solve |解决 |
| 444 | soluble |a 能解决的，可溶解的 |
| 445 | lease |v & a 出租（松开） |
| 446 | release |释放，豁免，发表 |
| 447 | leisure |n 休闲，空闲 |
| 448 | ancestor |n 祖先； |
| 449 | once |从前 |
| 450 | ancient |a 古老的 |
| 451 | antenna |n 天线 |
| 452 | ance-词根： |前 |
| 453 | ante-前缀： |前 |
| 454 | anticipate |预测；预料 |
| 455 | antique |a&n 古式的；过时的 |
| 456 | anecdote |n 轶事趣闻 |
| 457 | edit |编辑 |
| 458 | editor |n 编辑器 |
| 459 | dote-词根 |出版 |
| 460 | editorial |编辑的，评论的，社论的 |
| 461 | contradict |v 反驳 |
| 462 | contra-前缀 |相对的 |
| 463 | dict-词根 |说 |
| 464 | diction |措辞 |
| 465 | action |行为 |
| 466 | dictionary |字典 |
| 467 | discount |打折 |
| 468 | count |计算 |
| 469 | counter |计算器，柜台，相对的 |
| 470 | countra-前缀 |相对的 |
| 471 | contradict |反驳，抵触 |
| 472 | verdict |定论，裁决 |
| 473 | indicate |v 暗示，指出 |
| 474 | decicate |v 宣誓，奉献 |
| 475 | devote |v 投入于，献身 |
| 476 | nounce-词根 |声音 |
| 477 | announce |v 正式发布；发广播 |
| 478 | pronounce |发音 |
| 479 | pronunciation |n 发音 |
| 480 | denounce |谴责，公开指责 |
| 481 | ann-词根 |年 |
| 482 | annual |每年‘年刊 |
| 483 | perennial |a 四季不断，终年的 |
| 484 | ordin-词根 |次序 |
| 485 | order |命令 |
| 486 | coordinate |同等的（共同次序的） |
| 487 | subordinate |次要的，下等的 |
| 488 | anonymous |a 匿名的 |
| 489 | famous |著名的 |
| 490 | unfamous |不著名的 |
| 491 | name |名字 |
| 492 | fame |名声 |
| 493 | ornament（or更换） |v 装饰，美化 n 装饰装饰物 |
| 494 | adorn |n 装饰 |
| 495 | unanimous |全体一致的 |
| 496 | nominal |a 名义上的 |
| 497 | nominate |v 提名 |
| 498 | anxiety |n 挂念，焦虑，渴望 |
| 499 | anx-词根： |急 |
| 500 | anct | |
| 501 | ang-词根： |怒 |
| 502 | anger |愤怒 |
| 503 | angery |愤怒的 |
| 504 | appall |v 使惊骇 |
| 505 | pale |苍白 |
| 506 | palm |手掌 |
| 507 | apparatus |n 器械 器具 设备 |
| 508 | par-prepare |准备 |
| 509 | apparent |a 表面上的，显然的 |
| 510 | appear |出现 |
| 511 | disappear |消失 |
| 512 | transparent |a 透明的，透光的 |
| 513 | transport |运输 |
| 514 | appeal |v 呼吁，请求 n呼吁，申诉 |
| 515 | plea-词根 |请求 |
| 516 | please |请 |
| 517 | compulsory |a 必须做的，强制的 |
| 518 | puls-词根 |推 |
| 519 | pull |拉扯 |
| 520 | push |推 |
| 521 | impulse |n 推动，冲动 |
| 522 | pulse |n 脉搏 |
| 523 | expel |v 把。。。开除 |
| 524 | palm |掌 |
| 525 | expel |开除，发射 |
| 526 | prepel |推动，推进 |
| 527 | appendix |n 附属，附属物 |
| 528 | pend-词根 |挂、靠 |
| 529 | depend |依靠 |
| 530 | index |（ix）索引 |
| 531 | dig-词根 |手指 |
| 532 | digit |手指头 |
| 533 | digital |数字的 |
| 534 | pendulum |n 摆，钟摆 |
| 535 | appliance |n 电器，器械器具，适用 |
| 536 | apply |应用 申请 |
| 537 | employ |雇佣 |
| 538 | hire |雇佣（高级人才） |
| 539 | empolyer |雇主 |
| 540 | employee |雇员 |
| 541 | applicable |可应用的 |
| 542 | application |应用，申请 |
| 543 | exploit |开发，利用 |
| 544 | road | |
| 545 | throat |窄路；喉咙 |
| 546 | roach-词根： |路 |
| 547 | approach |v 靠近，接近 n 接近；途径 |
| 548 | local |当地的 |
| 549 | locate |给。。。定位 |
| 550 | location |地点 |
| 551 | place |地方 |
| 552 | raise |举起 |
| 553 | praise |表扬 |
| 554 | reproach |v、n 责备，指责 |
| 555 | approximate |近似 |
| 556 | arbitrary |a 武断的 |
| 557 | arbitr--- | |
| 558 | arbiter |法官；仲裁人 |
| 559 | arbit-词根 |法律 |
| 560 | arch |拱门 |
| 561 | angle |角 |
| 562 | arc |弧形 |
| 563 | architect |n 建筑师 |
| 564 | architecture |建筑学 |
| 565 | preotect |保护 |
| 566 | detect |发觉，发现 |
| 567 | tect-词根 |盖子 |
| 568 | target |目标；靶子 |
| 569 | hieratchy |n 统治集团，领导层，等级制度 |
| 570 | higher |更高的 |
| 571 | monarch |n 帝王，君主，最高级 |
| 572 | mono-前缀 |独 |
| 573 | tone |n 声调 |
| 574 | tune |v 曲调 |
| 575 | turn | |
| 576 | monotonous |单调的 |
| 577 | arch-词根 |主要的 |
| 578 | arise |v 出现，发生，起因于 |
| 579 | rise |升起 |
| 580 | raise |举起 |
| 581 | arouse |v.  唤起; 激起...的情欲; |
| 582 | rouse |v.  唤起; 激起...的情欲; |
| 583 | surge |v 汹涌；澎湃 n 巨浪 |
| 584 | source |n 源头 |
| 585 | resource |n 资源 |
| 586 | ridge |n.  背脊, 屋脊, 山脊 v.  使成脊状; 作垄; 装脊; 成脊状 rise+edge | 
| 587 | else=edge | |
| 588 | wedge |楔子 |
| 589 | wood | |
| 590 | hedge |篱笆 |
| 591 | pledge |誓言 |
| 592 | arithmetic |n 算数，四则运算 a 算数的 |
| 593 | arrange |n 安排 |
| 594 | array |n 排列 v 列阵，排列 |
| 595 | range |排列 |
| 596 | orange |橘子 |
| 597 | spring |喷泉 |
| 598 | spray |喷雾 |
| 599 | swing |秋千 |
| 600 | sway |摇摆 |
| 601 | cling |粘紧 |
| 602 | clay |黏土 |
| 603 | arrest |n 逮捕 v 逮捕 |
| 604 | rest  |休息 |
| 605 | rust |n.  锈, 生锈 v.  生锈, 衰退; 使生锈; 使荒废; 使变迟钝 | 
| 606 | arrogant |a 傲慢的自大的 |
| 607 | ar |一再地 |
| 608 | roar |n v 吼叫 |
| 609 | groan |v n 呻吟 |
| 610 | uproar |n 骚动，喧嚣，鼎沸 |
| 611 | crowd |n 人群 v 聚集；拥挤 |
| 612 | crow |n 乌鸦 啼叫，鸡叫 |
| 613 | crown |皇冠 |
| 614 | article |n 文章 |
| 615 | articulate |a 善于表达的 v清楚的讲话 |
| 616 | passion |激情 |
| 617 | eloquent |雄辩地 |
| 618 | chart |图标 |
| 619 | cartoon |卡通 |
| 620 | oon |圆形 |
| 621 | spade |铁锹 |
| 622 | spoon |勺 |
| 623 | balloon |气球 |
| 624 | moon |月亮 |
| 625 | charter |v 租船，租车 n 宪章，特许状 |
| 626 | cordial |n 兴奋剂 a 热忱的 |
| 627 | cardinal |n 红衣主教 a 核心的，首要的 |
| 628 | heart |cart 红心 |
| 629 | corn |域名 |
| 630 | horn |牛角 |
| 631 | unicorn |独角兽 |
| 632 | hell | |
| 633 | cellar |地窖 |
| 634 | shriek |尖叫 |
| 635 | scream |尖叫 |
| 636 | aspect |n 样子，外表，外貌 |
| 637 | spect-词根 |看 |
| 638 | inspect |视察 |
| 639 | expect |盼望 |
| 640 | prospect |前景 |
| 641 | respect |尊重 |
| 642 | peek |一撇 |
| 643 | ee、oo |眼睛 |
| 644 | peep |n.  吱吱声, 啾啾声; 汽车喇叭声; 嘀咕声, 轻轻的抱怨声#窥视; 一瞥; 偷看; 初现, 隐约显现 v.  吱吱叫; 嘀咕; 窥, 偷看; 隐约显现, 缓缓出现; 使出现 | 
| 645 | perspective |n 透视画法 a 透视的，透视画法 |
| 646 | prospect |n 景色；前景 |
| 647 | respective |a 各自的，各个的 |
| 648 | respectful |毕恭毕敬的 |
| 649 | respectable |可敬的 |
| 650 | spectrum |n 谱，光谱 |
| 651 | um |物理学名词后缀 |
| 652 | speculate |v 思索；推测 |
| 653 | vise-词根 |看 |
| 654 | wise |明智 |
| 655 | suspect |v 怀疑 a 可疑的 n 嫌疑犯 |
| 656 | sub-前缀 |下面 |
| 657 | suspicious |a 可疑的，多疑的 |
| 658 | conspicuous |a 有目共睹的 |
| 659 | apparent |显然的 |
| 660 | sceptical |a 怀疑论的 |
| 661 | skeptical |a 怀疑论的 |
| 662 | spec | |
| 663 | spic | |
| 664 | sceptical | |
| 665 | scope | |
| 666 | telescope |望远镜 |
| 667 | microscopic |围观的 |
| 668 | macroscopic |肉眼可见的，巨观的 |
| 669 | optic |眼的，光学的 |
| 670 | optical |眼的，光学的 |
| 671 | optics |光学 |
| 672 | optimistic |a. 乐观主义的 |
| 673 | pessimistic |悲观的 |
| 674 | optimum |最优值 |
| 675 | maximum |最大值 |
| 676 | minimum |最小值 |
| 677 | macro |大 |
| 678 | mini |小 |
| 679 | number |数字的 |
| 680 | conspiracy |n 阴谋，密谋 |
| 681 | pore |气孔毛孔 |
| 682 | bore |钻孔；无聊 |
| 683 | pare | |
| 684 | spire |呼吸 |
| 685 | inspire |吸气；鼓舞；打气 |
| 686 | expire |呼气，断气；到期 |
| 687 | conspire |串通一气，合谋 |
| 688 | porch |n 门廊，入口处 |
| 689 | port |港口 |
| 690 | hat | |
| 691 | hatch |舱盖 |
| 692 | semester |n 学期 |
| 693 | six |六 |
| 694 | sexth-拉丁文 |六 |
| 695 | assassinate |v 暗杀，行刺 |
| 696 | slaughter |屠杀 |
| 697 | massacre |大屠杀 |
| 698 | murder |谋杀 |
| 699 | kill |杀害 |
| 700 | harassment |n 骚扰 |
| 701 | harass |v 烦恼 （hare-ass） |
| 702 | hare |野兔 |
| 703 | ass |驴 |
| 704 | hear |听 |
| 705 | rabbit |兔子 |
| 706 | rapid |快的 |
| 707 | ambassador |n 大使；特使，代表 |
| 708 | amb-词根 |走 |
| 709 | ambulance |救护车，战事流动医院 |
| 710 | assault |v 猛烈的进攻 n 突然而猛烈 |
| 711 | sault-sult |跳 |
| 712 | sault（sal）-词根 |跳 |
| 713 | salient |易见的，跳跃的 |
| 714 | insult |侮辱，凌辱 |
| 715 | council |n 理事会；委员会顾问班子 |
| 716 | counsel |v/n 劝告 忠告 |
| 717 | consult |向…请教，向…咨询 |
| 718 | consultant |n 顾问 |
| 719 | counsel |忠告 |
| 720 | reconcile |v 使和好，调节 |
| 721 | assemble |vi 召集；集合 |
| 722 | assembly |adv 集合的 |
| 723 | as+same+place | |
| 724 | same | |
| 725 | sem/sim | |
| 726 | sym | |
| 727 | assimilate |v 吸收，消化 |
| 728 | similar |相似的 |
| 729 | simultaneous |a 同时的 |
| 730 | tane=time | |
| 731 | symmetry |n 对称的；匀称 |
| 732 | geometry |几何 |
| 733 | metre |米 |
| 734 | sympathy |n 同情，同情心 |
| 735 | pathetic |可怜的 |
| 736 | passion |激情 |
| 737 | path-词根 |情感 |
| 738 | symposium |n 讨论会，专题 |
| 739 | pot |罐子，坛子 |
| 740 | bottle |瓶子 |
| 741 | forum |论坛 |
| 742 | symptom |n 症状的 征兆 |
| 743 | symbol |n 符号，象征 |
| 744 | same | |
| 745 | syndrome |综合征 并发症 |
| 746 | asset |n 资产 |
| 747 | assess |v 评估 |
| 748 | sess-词根 （sit） |坐 |
| 749 | session |开会 |
| 750 | obsess |着迷 （ob一直） |
| 751 | obsession |n 着魔 萦绕 |
| 752 | census |n 人口普查 |
| 753 | possess |v 占有，拥有 |
| 754 | pose-词根 |放下 |
| 755 | seize |v 抓住，逮住 |
| 756 | siege |n 包围，围攻，围困 |
| 757 | assign |v 派给，分配 |
| 758 | resign |v 辞职 |
| 759 | sign-词根 |标志 |
| 760 | signature |n 签名 |
| 761 | significance |n 意义，含义 |
| 762 | significant |a 意义，含义 |
| 763 | signify |v 标识，意味；重要 |
| 764 | assist |v 协助，帮助 |
| 765 | assistant |n 助理 |
| 766 | sist-词根 |站 |
| 767 | stat-词根（stand） |站 |
| 768 | sist-词根 |站 |
| 769 | consistent |a 前后一致的，符合 |
| 770 | persist |v 持久，持续 |
| 771 | insist |v 坚决要求，坚决主张 |
| 772 | transistor |n 晶体管 |
| 773 | resist |v 抵制 |
| 774 | constituent |n 选民；成分 |
| 775 | constitute |v 组成 建立政府 制定法律 |
| 776 | institute |v 设立，设置 n 学会 学院 | 
| 777 | substitute |n 代替者；替身 v 代替，替换 | 
| 778 | superstition |n 迷信 |
| 779 | state |n 处境，情况 v 陈述，说明 | 
| 780 | statist |国治主义者；统计学家 |
| 781 | statistic |统计学的 |
| 782 | statistical |统计学的，统计学家的 |
| 783 | statistics |统计学 |
| 784 | economics |经济学 |
| 785 | dent-词根 |牙齿 |
| 786 | dentist |牙科医生 |
| 787 | statute |n 成文法，法令 |
| 788 | constitute |v 指定（法律）组成，任命 |
| 789 | assume |v 采取；假定 |
| 790 | summer |春天 |
| 791 | sume-词根 |抓、摘 |
| 792 | spring |秋天 |
| 793 | sprinkle |撒 |
| 794 | harvest |收割 |
| 795 | carve |砍 |
| 796 | winter |冬天 |
| 797 | consume |v 消耗，吃完 |
| 798 | summary |n 摘要，概要 |
| 799 | summarize |v 概括，总结 |
| 800 | sum |总和 |
| 801 | astronaut |n 太空人，宇航员 |
| 802 | nav-词根 |舟 |
| 803 | star |星星 |
| 804 | aster-词根 |星星 |
| 805 | astro-前缀 |星星 |
| 806 | av-词根 |鸟 |
| 807 | aviary |大型鸟舍 |
| 808 | aviation |航空，飞行 |
| 809 | navigation |航海 |
| 810 | fly |飞 |
| 811 | flow |流 |
| 812 | wing |n.翼，翅膀 |
| 813 | swim |游 |
| 814 | navy |海军 |
| 815 | naval |航海的 |
| 816 | army |军队 |
| 817 | navy |海军 |
| 818 | catastrophe |n 大灾难 |
| 819 | phenomenon |灾难 |
| 820 | disaster |n 灾难 |
| 821 | doom |劫数 噩兆 |
| 822 | occasion |机会，时机 |
| 823 | cas-chance |机会 偶然 |
| 824 | casual |偶然的 |
| 825 | case |情况 |
| 826 | stare |v 盯，凝视 |
| 827 | glare |瞪着 |
| 828 | startle |v 惊讶，惊吓 |
| 829 | band |布带 |
| 830 | bound |绑缚的 |
| 831 | stand | |
| 832 | astound |惊讶 |
| 833 | astonish |惊讶 |
| 834 | stun |惊讶 vt.使震惊，使目瞪口呆；打昏，使昏迷 |
| 835 | starve |饥饿 |
| 836 | atmosphere |n 大气（层） |
| 837 | sphere |n 球体 |
| 838 | hemisphere |n 半球 |
| 839 | stere |立体 |
| 840 | semiconductor |半导体 |
| 841 | semi-半（半) | |
| 842 | semi | |
| 843 | hemi s--h | |
| 844 | spin |v.旋转；晕眩；纺，织；甩干 n.旋转 |
| 845 | spine |n 脊柱，脊椎，书籍 |
| 846 | spiral |a 螺旋形的 n 螺旋 v 螺旋上升 |
| 847 | roll |滚动，滚筒 |
| 848 | rot-词根 |旋转 |
| 849 | rotate |v 旋转 |
| 850 | attach |v 系，贴，装 |
| 851 | detach |拆下，解开 |
| 852 | tach-词根 |系 |
| 853 | tight |紧的 |
| 854 | tie |领带，系 |
| 855 | stake |n 桩，树桩 |
| 856 | attempt |n.／ vt.尝试，企图，努力 |
| 857 | tempt |诱惑 vt.吸引，引起…的兴趣；引诱，诱惑 |
| 858 | temptation |n.诱惑，引诱 |
| 859 | contempt |n 轻视，藐视 |
| 860 | damn |诅咒 |
| 861 | condemn |谴责 |
| 862 | contemn |v 蔑视 |
| 863 | contempt |n 蔑视 |
| 864 | attend |vt.出席,参观;照料 vi.(to)专心于 |
| 865 | tend-词根 |伸 |
| 866 | ted-词根 |伸 |
| 867 | intend |企图 |
| 868 | tedious |a 冗长的，乏味的 |
| 869 | tedium |n.冗长乏味 |
| 870 | tense |绷紧的 |
| 871 | intense |剧烈的，强烈的 |
| 872 | nervous |神经紧张 |
| 873 | dense |a 密集的，浓厚的 |
| 874 | condense |v 压缩，冷凝 |
| 875 | density |密度 |
| 876 | attidude |n 态度 |
| 877 | alt-词根 |高 |
| 878 | latitude |n 海拔高度 |
| 879 | longigude |n 经度 |
| 880 | multitude |n 众多 |
| 881 | attorney |n 业务律师，辩护人 |
| 882 | turn |转交 |
| 883 | attorn |vt.  转让, 让予 vi.  承认新地主 | 
| 884 | torn |v.  扯; 撕破; 撕; 被撕裂; 飞跑; 被扯破; 狂奔; 流泪; 含泪 |
| 885 | torch |n 火把，手电筒 |
| 886 | tort |n.  民事侵权行为 |
| 887 | torment |n 痛苦，折磨 |
| 888 | torrent |n 激流，洪流 |
| 889 | current |n 水流，潮流，气流，电流 |
| 890 | torture |v、n 拷问 |
| 891 | tunnel |n 隧道，山洞 |
| 892 | tun |n 大酒桶 v 装入酒桶 |
| 893 | channel |n 海峡，频道 |
| 894 | turbine |n 汽轮机，涡轮机 |
| 895 | turbulent |a 吵闹的，狂躁的 |
| 896 | trouble-tubul | |
| 897 | term |n 学期，期限；条件；术语 |
| 898 | term-词根 |终点 |
| 899 | terminate |到终点 |
| 900 | attribute |v 归因于，归属于 n 属性，品质 | 
| 901 | contribute |捐赠，捐献 |
| 902 | distribute |分发，分布 |
| 903 | tribute |n 贡品，颂词；贡物 |
| 904 | gift |礼物（give） |
| 905 | tribe |n.  部落, 部族 |
| 906 | tri-being | |
| 907 | auction |n、v 拍卖 |
| 908 | aux | |
| 909 | aucs | |
| 910 | aug-词根 |高大 |
| 911 | augment |v 增大 提高 |
| 912 | augmentor |n.  起扩大作用的人; 推力增强装置 |
| 913 | auxiliary |a 辅助的 补助的n 辅助者 |
| 914 | authentic |a 真的，真正的 |
| 915 | author |n 作家 创造者 |
| 916 | authority |n 著作权威 |
| 917 | authentic |n 具有权威性的 |
| 918 | audience |n 听众 观众 |
| 919 | oral-词根 |口头的 |
| 920 | aural |听觉的 |
| 921 | ear | |
| 922 | aud-词根 |听 |
| 923 | radio | |
| 924 | audio | |
| 925 | video | |
| 926 | audit |v 旁听；审计；查账； |
| 927 | teller |出纳 |
| 928 | deficit |赤字 |
| 929 | auditorium |n 听众席，观众席 |
| 930 | museum |博物馆 |
| 931 | stadium |n.  露天大型运动场 |
| 932 | gymnasium |体育馆 |
| 933 | auditorium |听众管 |
| 934 | aquarium |水族馆 |
| 935 | available |a 可用的 |
| 936 | invalid |n 病人，伤残 a 有病的 |
| 937 | invaluable |a 非常宝贵的 |
| 938 | tuition |n 学费 |
| 939 | intuitioin |直觉 |
| 940 | intuitive |只觉得 |
| 941 | tuit-词根： |学 |
| 942 | tut-词根 |教 |
| 943 | tutor |家庭教师 |
| 944 | renaissance |复兴，文艺复兴 |
| 945 | nais |生 |
| 946 | nad | |
| 947 | nat nation |国家民族 |
| 948 | nature |自然 |
| 949 | vail |价值 |
| 950 | nat |生 |
| 951 | nais |生 |
| 952 | avert |v 防止，避免 |
| 953 | convert |v 转变（信仰）兑换 |
| 954 | vertical |adj 垂直的 |
| 955 | straight |笔直的 |
| 956 | hover |盘旋 |
| 957 | hawk-ver | |
| 958 | sovereign |n.  皇帝, 君主, 金磅 adj.  至高无上的, 独立自主的, 君主的 | 
| 959 | super | |
| 960 | reign |统治 |
| 961 | foreign |外国的 |
| 962 | versatile |a 通用的，多才多艺的 |
| 963 | controversy |n 争辩 |
| 964 | contro- |相对 |
| 965 | reverse |n 反转、颠倒 n 反面，背面 a 颠倒的 |
| 966 | verge |n 边，边缘 v 濒临 |
| 967 | edge |n 边缘 |
| 968 | awe |n 敬畏，惊惧 v 使敬畏 |
| 969 | awful |a 可怕的，威严的，糟糕的 |
| 970 | awkward |a 使用不便的，笨的 |
| 971 | stupid |a 人傻的 |
| 972 | awk-词根 |反 |
| 973 | outward |adj.  外面的, 公开的, 外表的 adv.  向外, 表面, 在外 | 
| 974 | inward |n.  内部, 里面; 肠胃; 内脏 adj.  内心的, 本来的, 向内的 adv.  向内; 在内 | 
| 975 | forward |n.  期货, 序言, 发送; 前锋 v.  转寄, 发送, 运送; 促进, 推进; 把一封收到的电子邮件再寄给别人 (计算机用语) adj.  向前的, 迅速的, 早的 adv.  向前地 | 
| 976 | bachelor |n 单身汉 |
| 977 | beat |打折 |
| 978 | bat（bach） |棒子 |
| 979 | or |士 |
| 980 | bacterium |n 细菌 |
| 981 | bacon |n 培根，熏肉 |
| 982 | bake |烘焙 |
| 983 | cake |蛋糕 |
| 984 | badge |n 徽章，奖章 |
| 985 | blaze |n 火焰，光辉 |
| 986 | blast |爆炸 |
| 987 | burst |爆炸 |
| 988 | burn |火 |
| 989 | bait |n 饵，引诱物 v 用饵引诱 |
| 990 | bite |咬 |
| 991 | abide |忍受，坚持 |
| 992 | bald |a  秃的，秃头的 |
| 993 | ball |球 |
| 994 | bare |赤裸的 |
| 995 | bear |生育，熊 |
| 996 | born |生 |
| 997 | bold |a 粗的，大胆的 |
| 998 | bole |树干 |
| 999 | bone |骨头 |
| 1000 | banner |旗帜，横幅 |
| 1001 | band | |
| 1002 | slogan |标语 |
| 1003 | logo |标识 |
| 1004 | bound |绑缚的 边界 蹦跶 |
| 1005 | bond |n 结合物；债券 |
| 1006 | bundle |n 一束 一捆 |
| 1007 | bonus |n 奖金，红利 |
| 1008 | banquet |n 宴会 v 参加宴会 |
| 1009 | bench |n 长条凳 |
| 1010 | breach |n 违反 v 冲破 公婆 |
| 1011 | break | |
| 1012 | barber |n 理发师 |
| 1013 | beard |胡子 |
| 1014 | exhibit |v 展出 |
| 1015 | inhibit |v 抑制 |
| 1016 | prohibit |v 禁止 阻止 |
| 1017 | hibit |持有 |
| 1018 | barn |n 谷仓 |
| 1019 | cabin |n 小木屋；船舱 |
| 1020 | barren |a 贫瘠的；不育的 |
| 1021 | bare |v、a赤裸裸 |
| 1022 | born |生育（过） |
| 1023 | bear |熊；生育 |
| 1024 | hurl |v 猛投 |
| 1025 | acrobat |n 特技演员 |
| 1026 | acro |大 |
| 1027 | macro |大 |
| 1028 | micro |小 |
| 1029 | beneath |prep 在。。。下面 ad在下方 |
| 1030 | next |下一个 |
| 1031 | wreath |n 花环，花圈 |
| 1032 | wrist |手腕 |
| 1033 | waist |腰 |
| 1034 | myth |n 深化 |
| 1035 | mist |n 雾 |
| 1036 | mystery |a 神秘的 |
| 1037 | benign |a 良性的，仁慈的 |
| 1038 | bizarre |a 异乎寻常的 外貌特殊 |
| 1039 | blade |n 片 |
| 1040 | flat |n 扁平的 |
| 1041 | spade |铁锹 |
| 1042 | spoon |汤匙 |
| 1043 | fraction |n 碎片，小部分 |
| 1044 | fracture |n 裂缝 |
| 1045 | fragile |a 易碎的 |
| 1046 | fragment |n 碎片，小部分 |
| 1047 | frag-词根 |破 |
| 1048 | bloom |n 花 v花开 |
| 1049 | flower |花朵 |
| 1050 | blossom |花簇 |
| 1051 | blame |v 责备 |
| 1052 | complain |抱怨 |
| 1053 | explain |解释 |
| 1054 | fame |名称（词根：说） |
| 1055 | name |名字 |
| 1056 | famous |著名的 |
| 1057 | fable |语言 |
| 1058 | infant |婴儿 |
| 1059 | orphan |孤儿 |
| 1060 | prominent |a 突出的 凸起的 |
| 1061 | eminent |a 突出的 凸起的 |
| 1062 | prominence |a 突出 凸起 |
| 1063 | eminence |a 突出 凸起 |
| 1064 | min（mountain） |凸起，突出 |
| 1065 | undermine |v 暗中破坏 挖墙脚 |
| 1066 | mine |n 矿山 v采矿 |
| 1067 | menace |v、n 危险，威胁，威吓 |
| 1068 | survey |v、n 俯瞰，眺望；全面审视，调查 |
| 1069 | supervise |监督，监考 |
| 1070 | vision |视觉 |
| 1071 | visible |可见的 |
| 1072 | invisible |adj.  看不见的; 无形的 |
| 1073 | blaze |n 火焰，火光；闪光 |
| 1074 | blend |n 混合物 v 混合，混杂 |
| 1075 | bland |平淡 |
| 1076 | bouse |n 女衬衫，短上衣 |
| 1077 | blow+dress | |
| 1078 | prudent |a 审慎的 |
| 1079 | provident |a 有远见的 |
| 1080 | providence |a 远见卓识 |
| 1081 | crew |n 全体成员，全体船员 |
| 1082 | grow |成长 |
| 1083 | recruit |v 招募（新兵），吸收 n 新成员，新兵 |
| 1084 | blunder |v 犯大错 n 大错 |
| 1085 | blind |瞎的 |
| 1086 | blunt |a 率直的 v 迟钝 |
| 1087 | blur |n 模糊不清 |
| 1088 | blear |v 模糊 |
| 1089 | clear |v.  澄清, 清除障碍; 变干净; 变晴; 变清澈; 变清楚 adj.  清澈的, 透明的; 明亮的; 洁净的; 晴朗的 adv.  清楚地; 完全地 n.  清除, 消除; 不做记号的行为, 从检查方块里消除一个记号 (计算机用语) | 
| 1090 | spur |n 马刺 v 刺激，激励 |
| 1091 | spear |矛 |
| 1092 | pierce |v.  刺穿, 洞悉, 穿透; 穿入, 透入, 进入 |
| 1093 | blush |v、n 脸红 |
| 1094 | blood |血 |
| 1095 | flush |n 奔流 |
| 1096 | flow+rush | |
| 1097 | bosom |n.  胸部; 胸怀; 胸 adj.  知心的, 亲密的 | 
| 1098 | bosom friend |知心朋友 |
| 1099 | bounce |n.  跳, 弹力, 跳跃 v.  反跳, 弹跳; 使跳回, 使撞击 | 
| 1100 | bound |蹦跶；束缚的，边界 |
| 1101 | bowel |n 肠；内部，深处 |
| 1102 | bow |弓形 |
| 1103 | elbow |肘 |
| 1104 | arrow |箭 |
| 1105 | brace |v 使防备；支撑 （四肢）n 托架 |
| 1106 | bracket |n.  支架, 托架, 括弧 v.  装托架, 括入括弧 | 
| 1107 | branch |树枝 |
| 1108 | embrace |拥抱 |
| 1109 | racket |球拍 |
| 1110 | bureau |n.  办公桌 办公处  |
| 1111 | bureaucracy |n 官僚主义 |
| 1112 | democracy |n 民主 |
| 1113 | breed |v 繁殖，生殖；产生，教养，抚养 |
| 1114 | brood |n 一窝 |
| 1115 | brother |兄弟 |
| 1116 | food | |
| 1117 | blood | |
| 1118 | feed | |
| 1119 | bleed | |
| 1120 | brittle |a 易碎的，脆弱的 |
| 1121 | break |破碎 |
| 1122 | bruise |青，肿的 |
| 1123 | blue | |
| 1124 | breeze |n 微风，轻而易举 |
| 1125 | breath |n.  呼吸; 气味; 气息 |
| 1126 | breathe |v.  呼吸; 吸气; 呼气; 通气, 透气; 呼吸; 吸入; 呼出; 使喘息 |
| 1127 | bribe |n 贿赂 v 行贿 |
| 1128 | bread beggaar | |
| 1129 | bride |n 新娘 |
| 1130 | briefcase |n 手提箱 |
| 1131 | case |盒子 |
| 1132 | cash | |
| 1133 | cage |n 鸟笼 |
| 1134 | brilliant |a 光辉的 |
| 1135 | brilliance |n 光辉的 |
| 1136 | brell-词根 |光明 |
| 1137 | brill |光 |
| 1138 | unbrella |伞 |
| 1139 | ring |环 戒指 |
| 1140 | rim |轮缘，篮筐 |
| 1141 | bowl |碗 |
| 1142 | brim |n 边缘，帽檐；碗边 |
| 1143 | brisk |a 轻快的，生机勃勃的 |
| 1144 | music |brisk |
| 1145 | brochure |n 小册子 |
| 1146 | clergy |n 牧师，神职人员 |
| 1147 | c-learned-y |有学问的人 |
| 1148 | clerk |n 职员 |
| 1149 | wit |n 智力 |
| 1150 | wise |n.  方法, 方式#聪明的人 adj.  英明的, 慎虑的, 明智的#有智慧的 | 
| 1151 | spirit |精神 |
| 1152 | witness |n.  证人, 证据, 目击者 v.  目击, 证明, 作证; 作证人, 成为证据 | 
| 1153 | wit-词根 |看，才智 |
| 1154 | client |n 律师，客户等 |
| 1155 | cline-词根 |斜靠 |
| 1156 | incline |v 倾斜 n斜坡 |
| 1157 | decline |v 下降 n 下降 |
| 1158 | cline-词根 |倾斜 |
| 1159 | jean |牛仔裤 |
| 1160 | lean |v 倾斜 |
| 1161 | client |委托人 |
| 1162 | clinic |n 诊所 |
| 1163 | crude  |adj.  天然的, 粗糙的, 未成熟的 |
| 1164 | rude |祖鲁的 |
| 1165 | brute |畜生 |
| 1166 | brutal |粗鲁的，残酷的 |
| 1167 | curl |v 使卷曲 n 卷发 |
| 1168 | circle | |
| 1169 | reptile |a 爬行的，卑鄙的 n 爬行动物 |
| 1170 | creep |爬行 |
| 1171 | crawl |如东 |
| 1172 | crab |螃蟹 |
| 1173 | cliff |n 悬崖，峭壁 |
| 1174 | climb |爬行 |
| 1175 | sniff |n.  吸, 吸气声, 闻 v.  嗅, 嗤之以鼻, 蔑视; 闻, 用力吸, 发觉 | 
| 1176 | snoopy |adj.  爱窥探的; 爱管闲事的（史努比） |
| 1177 | stiff |a 硬的，僵直 |
| 1178 | climax |n 顶点，高潮 |
| 1179 | cripple |n.  跛子; 残废的人 v.  使跛, 削弱, 使成残废 | 
| 1180 | crip-creep | |
| 1181 | disable |残疾人 |
| 1182 | crab | |
| 1183 | grab |抢夺 |
| 1184 | scramble |n.  爬行, 抢夺, 攀缘 v.  攀缘, 争夺, 杂乱蔓延; 攀登, 使混杂, 搅乱 | 
| 1185 | crawl |v、n 爬行 |
| 1186 | scramble | |
| 1187 | creep | |
| 1188 | claw |爪子 |
| 1189 | cloak |n 斗篷 vt 掩盖 |
| 1190 | coat |n.  外套 v.  外面覆盖, 穿外套 | 
| 1191 | clock |钟 |
| 1192 | closet |n.  壁橱, 小室 v.  关入私室密谈 | 
| 1193 | cluster |丛，群 v 群集 |
| 1194 | close |关门 |
| 1195 | clutch |v 抓住 n 离合器 |
| 1196 | clude-词根 |关门 |
| 1197 | conclude |v 结束，终止 |
| 1198 | conclusion | |
| 1199 | exclude |n 拒绝，排除 |
| 1200 | preclude |v 排除，阻止 |
| 1201 | clumsy |a 笨拙的，愚笨的 |
| 1202 | clown |小丑 |
| 1203 | coarse |a 醋澡的 |
| 1204 | course |过程 |
| 1205 | cur、curs | |
| 1206 | coil |v 卷，盘绕 |
| 1207 | collect |收集 |
| 1208 | coincide |vi 同时发生（地点、时间，时间，人物相同） |
| 1209 | casual |偶然的 |
| 1210 | accident |意外 |
| 1211 | incident |事件、事变 |
| 1212 | collaborate |vi 协作 |
| 1213 | oper-词根 |做 |
| 1214 | operate |v 做手术，操作，运转 |
| 1215 | operation |n 操作，做手术 |
| 1216 | slap |n、v 拍，击掌 |
| 1217 | flap |n.  拍打; 副翼; 拍打声 v.  拍打; 使拍动, 飘动; 拍击; 振; 拍动; 扑动; 飘动; 拍翅飞行 | 
| 1218 | lap |大腿前侧 |
| 1219 | lapse-词根 |失，落下 |
| 1220 | collapse |v、n 倒塌；跌 |
| 1221 | elapse |时光流逝 |
| 1222 | slip |v、n.  滑动; 失足 |
| 1223 | slope |n 斜坡 |
| 1224 | slippery |a 滑的 |
| 1225 | slipper |n.  浅口便鞋, 拖鞋; 船鞋 |
| 1226 | lapes |n.  过失, 失效, 流逝 v.  失检; 陷入; 背离; 流逝; 使失效 | 
| 1227 | elapse |v.  过去; 消逝（专指时间） |
| 1228 | lubr-词根 |滑 |
| 1229 | lubricate |v 润滑 |
| 1230 | lubricant |n 润滑剂 a 润滑的 |
| 1231 | collide |v 互撞，碰撞 |
| 1232 | lid |盖子 |
| 1233 | lip |唇 |
| 1234 | shield |盾 |
| 1235 | shell |贝壳 |
| 1236 | collision |n.  碰撞, 抵触, 冲突 |
| 1237 | colony |n 殖民地； |
| 1238 | colo- |繁殖 |
| 1239 | cult-词根 |种植 |
| 1240 | column |圆柱，柱状物 |
| 1241 | combine |联合 结合 |
| 1242 | com bi near | |
| 1243 | comedy |n 喜剧 |
| 1244 | comic |a 喜剧的 |
| 1245 | tragedy |n 悲剧 |
| 1246 | tragic |a 悲剧的 |
| 1247 | edy |剧烈的，强烈的 |
| 1248 | melody |旋律 |
| 1249 | holiday |假日 |
| 1250 | melon |n 甜瓜 |
| 1251 | lemon |柠檬 |
| 1252 | command |nv 命令，指令 n 掌握 |
| 1253 | mind-词根 |命令 |
| 1254 | demand |n.  要求; 需要; 需求 v.  要求, 请求; 查问, 盘诘; 需要; 传唤 | 
| 1255 | mandarin |n 官话，普通话，中国柑橘 |
| 1256 | comment |n 注释 |
| 1257 | ment |精神思想 |
| 1258 | mental |a 精神的 智力的 |
| 1259 | physical |a 身体的，肉体的  |
| 1260 | commend |称赞 |
| 1261 | commerce |n 商业 |
| 1262 | mercury |n.  水银, 使者, 汞（墨丘利） |
| 1263 | Mercury |n.  水星 |
| 1264 | commercial |商业的 |
| 1265 | merchant |商人 |
| 1266 | merchantise |经商；商品 |
| 1267 | merc- |商业的 |
| 1268 | commission |n.  佣金; 任务, 职权, 权限; 委任, 委托; 委员会 v.  委任, 使服役, 委托制作 | 
| 1269 | virgin | |
| 1270 | commit | |
| 1271 | mit-词根 |投，扔 |
| 1272 | miss | |
| 1273 | missile |导弹 |
| 1274 | omit |v 省略，删去 |
| 1275 | omission |n 冗长 |
| 1276 | submit |递交，呈交 |
| 1277 | transmit |v.  传输, 传达, 转送; 发射信号, 发报 |
| 1278 | compromise |n.  妥协 vt 妥协 vi 危及 | 
| 1279 | excuse | |
| 1280 | serious | |
| 1281 | promise | |
| 1282 | permit |v 许可 n 许可证 |
| 1283 | might |可以 可能 |
| 1284 | may |可以 |
| 1285 | main |主要的 |
| 1286 | mean |普通的 意思 |
| 1287 | common |一般的 公共的 |
| 1288 | communicate |v 沟通，通信 |
| 1289 | commute |乘公交车往返于 |
| 1290 | immune |免疫的 |
| 1291 | mutual |a 相互的，彼此的 |
| 1292 | local |当地的 |
| 1293 | locomotive |n 机车，火车头 a 运动的，移动的 |
| 1294 | motion | |
| 1295 | move | |
| 1296 | promote |v 促进，发扬 |
| 1297 | mute-词根 |移动 |
| 1298 | prompt |a 敏捷的 v 激起 |
| 1299 | mob |n 乌合之众 vi 围攻 |
| 1300 | mobile |a 可移动的 |
| 1301 | curve |曲线 |
| 1302 | curb |n.  抑制, 边石, 勒马绳 v.  抑制, 勒住, 束缚 | 
| 1303 | scar |n、v 刀疤 |
| 1304 | immigrant |a、n 移民（move+ground） |
| 1305 | emigrant |移居出境的人 |
| 1306 | migrate |v 迁移 |
| 1307 | compact |a 紧密的 v使紧凑 |
| 1308 | pact | |
| 1309 | pat |n 轻拍 |
| 1310 | plam |n.  手掌, 手心; 一手宽; 前足掌; 一手长#棕榈; 胜利; 棕榈叶; 荣誉勋章 v.  把...藏于手中; 与...握手; 用手掌触摸; 把...硬塞给 | 
| 1311 | compare |比较，对比 |
| 1312 | pair |一对 |
| 1313 | comparable |有可比性的 |
| 1314 | comparative |比较的 |
| 1315 | comparison |n.  比较; 比喻; 对照 |
| 1316 | parent |父母 |
| 1317 | para |n 伞兵 |
| 1318 | para-词根 |反，旁 （op） |
| 1319 | parachute |n 降落伞 v 跳伞 |
| 1320 | cate-前缀 | |
| 1321 | paradise |n 天堂 |
| 1322 | death | |
| 1323 | dead | |
| 1324 | dy | |
| 1325 | die | |
| 1326 | paradox |n 似非而是 |
| 1327 | doct | |
| 1328 | dict | |
| 1329 | doct | |
| 1330 | dox | |
| 1331 | parallel |a v 平行 相同 类似 |
| 1332 | paralyze |v 使瘫痪（麻痹） |
| 1333 | loose | |
| 1334 | paralyse | |
| 1335 | parameter |n 参数 |
| 1336 | parasite |n 寄生虫，食客 |
| 1337 | compass |n 罗盘，指南针 v 包围 |
| 1338 | com |共同 |
| 1339 | pass-词根 |走 |
| 1340 | pace |步伐 |
| 1341 | path |路 |
| 1342 | compassion |n 同情，怜悯 |
| 1343 | compatible |a 能和睦相处的 |
| 1344 | pass-词根 |情感 |
| 1345 | pathetic |a 可怜的 |
| 1346 | sympathy |n 同情，同情心；赞同 |
| 1347 | same | |
| 1348 | compensate |v 补偿，赔偿 |
| 1349 | pens-词根 |钱 |
| 1350 | money | |
| 1351 | penny |便士（泛指钱） |
| 1352 | expensive |昂贵的 |
| 1353 | pension |n 养老金，年金 |
| 1354 | expenditure |n 花费（时间，金钱） |
| 1355 | compile |vt 编辑，编制，搜集 |
| 1356 | edit |编辑，校订 |
| 1357 | pile |一堆 |
| 1358 | pole |杆子 |
| 1359 | bone |骨头 |
| 1360 | complex |a 复杂的 合成的 n 联合体 |
| 1361 | plex-词根 |叠，情节 |
| 1362 | flex-词根 |折 |
| 1363 | flexible |柔韧的，灵活的 |
| 1364 | flect-词根 |折 |
| 1365 | reflect |反射，反响，反应 |
| 1366 | complicate |v.  弄复杂, 使起纠纷, 使错综; 变复杂 |
| 1367 | complicated | |
| 1368 | perplex |v 使困惑 |
| 1369 | explicit |adj.  详尽的; 明确的; 清楚的; 直率的 |
| 1370 | implicit |a 含蓄的 |
| 1371 | imply |v 含。。。意思 |
| 1372 | compliment |n 问候 |
| 1373 | component |n 组成部分 |
| 1374 | pound-词根 |放 |
| 1375 | pon-词根 |放 |
| 1376 | put | |
| 1377 | pos | |
| 1378 | supersonic |a n 超音速的，超声波的 |
| 1379 | opponent |n 对手 a 对立的 |
| 1380 | oppose |v 反对，使对立 |
| 1381 | expose |曝光 |
| 1382 | exposure |n 暴露，揭露 |
| 1383 | deposit |v 存放，沉淀 n 存款 |
| 1384 | ponder |v 沉思，考虑 |
| 1385 | pound |磅 |
| 1386 | ponderous |沉重的 多。。。的 |
| 1387 | compound |n 混合物，化合物 |
| 1388 | composite |a 混合而成的 n 合成物 |
| 1389 | compress |v 压紧，压缩，浓缩 |
| 1390 | depress |压抑 |
| 1391 | depressed |压抑的 |
| 1392 | pressure |压力 压强 |
| 1393 | forece |力量 施加压力 |
| 1394 | press |压 |
| 1395 | nutrilite |营养品 |
| 1396 | nurt-词根 |营养品 |
| 1397 | nurture |养育，教育 |
| 1398 | comprise |v 包含，包括，由。。组成 |
| 1399 | contain |容纳，包含 |
| 1400 | comprehension |n.  理解; 包含 |
| 1401 | comprehend |v.  领会; 包括; 理解 |
| 1402 | grasp |n.  抓住; 掌握; 抓紧 v.  抓住, 领会, 紧握; 抓 | 
| 1403 | conceal |v 隐藏，隐瞒 |
| 1404 | seal |密封 |
| 1405 | heal |愈合 |
| 1406 | health |健康 |
| 1407 | reveal |v 展现，显示，揭示 |
| 1408 | veil |n 面纱，遮蔽物 v 用面纱遮盖，掩饰 |
| 1409 | willow |柳树 |
