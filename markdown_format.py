#!/usr/bin/python3 -w
import openpyxl
import os
import re
from openpyxl.worksheet.worksheet import Worksheet

class	XlsxToMarkdown(object):
	def ThransferFile(self,refer_excel,sheet):
		out_file_name = sheet + '.md'
		txt_out_file_name = sheet + '.txt'
		out_file = open(out_file_name,'w',encoding='utf-8')
		txt_out_file = open(txt_out_file_name,'w',encoding='utf-8')
		print('''| NUM |Word | Translation |''', file=out_file)
		print('''| ------ | ------ | ------ |''', file=out_file)

		#refer_excel = openpyxl.load_workbook('newwords.xlsx')
		#print(refer_excel.active)
		sheet: Worksheet =  refer_excel[sheet]
		minrow=sheet.min_row #最小行
		maxrow=sheet.max_row #最大行
		mincol=sheet.min_column #最小列
		maxcol=sheet.max_column #最大列
		for i in range(minrow,maxrow+1):
			# print(i)
			print("| ", end='', file=out_file)
			print(i, end='', file=out_file)
			print(" | ", end='', file=out_file)
            
			txt_input_word = sheet.cell(i,1).value
			if sheet.cell(i,1).value is None:
				continue
			elif re.search(u'/',  txt_input_word):
				# txt_input_word = re.sub(r'[\\.*\\]', '', txt_input_word)
				new_txt_input_word = re.sub('/.*/', '', txt_input_word)
				print(new_txt_input_word, end='\n', file=txt_out_file)
			elif re.findall(u'[Day|词根|=|-|（|\t]',txt_input_word):
				pass
			else:
				print(txt_input_word, end='\n', file=txt_out_file)

			for j in range(mincol,maxcol+1):
				cell=sheet.cell(i,j).value
				# print(type(cell))
				if cell is None:
				    print(" |", end='', file=out_file)
				else:
				    if '\n' in cell:
				        # print(str(cell))
				        print(cell.replace("\n", " "),end='', file=out_file)
				        print(" | ", end='', file=out_file)
				    else:
				        print(cell.replace("\n", " "),end='', file=out_file)
				        print(" |", end='', file=out_file)
			print(file=out_file)
							
		out_file.close()
	
	def GetAllSheet(self,filename):
		refer_excel = openpyxl.load_workbook(filename)
		for sheet in refer_excel.sheetnames:
			self.ThransferFile(refer_excel,sheet)
		os.system("git add -A && git commit -m 'update' && git push")


if __name__ == '__main__':
	p=XlsxToMarkdown()
	p.GetAllSheet('newwords.xlsx')
